function [ib,jb,vb] = asmneum(nbe,h,iele1,dispele,inode1,dispnode,w,a,pmlctx,npml)
%asmneum -- assembly for Neumann b.c. on one straight boundary
%
%  [ib,jb,vb] =
%  asmneum(nbe,h,iele1,dispele,inode1,dispnode,w,a,pmlctx,npml) computes
%  [i,j,v] of (sub)domain matrix for Neumann b.c. on one line segment in a
%  rectangle domain.  Only entries for interactions along the line are modified.
%
%  OUTPUTS
%
%  ib,jb,vb -- 4-by-nbe arrays for the sparse (sub)domain matrix, indexed
%  locally or globally
%
%  INPUTS
%
%  nbe -- number of boundary elements
%
%  h   -- mesh size that is tangential or normal to boundary
%
%  iele1 -- index in global physical domain, of the volume element that is
%  adjacent to the physical boundary
%
%  dispele -- displacement in global physical domain, of the volume element
%  along the physical boundary, assume positive
%
%  inode1 -- index in local or global domain of the 1st boundary node
%
%  dispnode -- displacement in local or global domain of the next boundary
%  node, assume positive
%
%  w -- array of wavenumber values, order by index in global domain
%
%  a -- array of density coefficients, -div(a grad)
%
%  pmlctx -- .side, .H, .h, .sgn, .nl, .sx, .sy

if dispele<0 || dispnode<0
    error('asmneum: 4th and 6th args must be positive');
end

%   element stiffness and mass stencils restricted along the interface 
%     2  3
%     | /   
%     0--1
%   only 0--1 are accounted for
hxy= h(1)/h(2);  hyx= 1/hxy;
Ks= [hxy+hyx; 0.5*hxy-hyx]/3.0;
Ms= [1/9.0; 1/18.0]*h(1)*h(2);


%   special case that nbe is zero, only one node
if nbe==0
    ib= [inode1;inode1];
    jb= [inode1;inode1+dispnode];
    vb= a(iele1)*Ks - w(iele1)^2*Ms;
    return;
end


%   for every element along the line, compute [ib,jb,vb]
ib= zeros(4,nbe); jb= ib; vb= ib;
if isempty(pmlctx)
    for ibe= 1:nbe
        ib(:,ibe)= inode1+(ibe-1)*dispnode+[0;0;dispnode;dispnode];
        jb(:,ibe)= inode1+(ibe-1)*dispnode+[0;dispnode;dispnode;0];
        ia= iele1+(ibe-1)*dispele;
        vb(:,ibe)= a(ia)*[Ks;Ks] - w(ia)^2*[Ms;Ms];
    end
else
    for ibe= 1:nbe
        ib(:,ibe)= inode1+(ibe-1)*dispnode+[0;0;dispnode;dispnode];
        jb(:,ibe)= inode1+(ibe-1)*dispnode+[0;dispnode;dispnode;0];
        if ibe>npml(1) && ibe<=nbe-npml(2)
            ia= iele1+(ibe-npml(1)-1)*dispele;
            vb(:,ibe)= a(ia)*[Ks;Ks] - w(ia)^2*[Ms;Ms];
        elseif ibe<=npml(1)
            ia= iele1;
            vb(:,ibe)= pmlctx.bmat(ibe-npml(1)-1,a(ia),w(ia),pmlctx);
        elseif ibe>nbe-npml(2)
            ia= iele1+(nbe-npml(1)-npml(2)-1)*dispele;
            vb(:,ibe)= pmlctx.bmat(ibe-nbe+npml(2),a(ia),w(ia),pmlctx);
        end
    end    
end