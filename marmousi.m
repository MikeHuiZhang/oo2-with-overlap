% $$$ myDomain= [0 9216; 0 2928];               % xl, xr, yl, yr
% $$$ file4vel= 'data/marmhard.txt';            % empty to use default value one
% $$$ velsize= [384 122];                       % size of c in x,y
myDomain= [0 2301; 0 751]*4;
file4vel= 'data/marmousi_velocity';
fileform4vel= 'bin';
velsize= [2301 751];
file4a= [];                             % empty to use default value one
scpoints= [6100,2200];%[6100,2200; 3100, 2200; 100, 2200];% locations of point sources
Sp= 1;                                  % strength of point sources
sclines= [];%[.1 .1 .9 .1];             % line sources, (x1,y1,x2,y2) 
Sl= 1;
x0= 0.5; y0= 0.5; eta= 0.5;             % area source: Gaussian 
Sa= [];%@(x,y)cos(pi*x).*cos(pi*y);%exp(-((x-x0).^2+(y-y0).^2)/(2*eta^2))/(2.0*pi*eta^2); 
gD= [];%@(x,y) cos(2*pi/20*y).*exp(-1i*sqrt((2*pi*f)^2-(2*pi/20)^2)*x); 
           % uefun(x,y,scpoints,2*pi*f,sgn);
           % Dirichlet data
