# README #

Here are the codes used for the paper "optimized Schwarz methods with overlap for the Helmholtz equation".
The codes are written in Matlab. It depends on two external packages: [Factorize](http://www.mathworks.com/matlabcentral/fileexchange/24119-don-t-let-that-inv-go-past-your-eyes--to-solve-that-system--factorize-) and [Sparse sub access](http://www.mathworks.com/matlabcentral/fileexchange/23488-sparse-sub-access)

### Run the codes ###

* many things are defined in helm2drand.m and helm2dsrc.m
* run the script runscale.m

### Note! ###

* research codes, not fast
* solve 2-D Helmholtz on a rectangle with spatially varying wave speed and density
* the gmres function of Matlab gives the residual of the preconditioned system, to get the residual of the original system and restricted to the physical domain, one needs to write a new one; we did this by modifying the gmres.m from Matlab but we are not allowed to publish it here