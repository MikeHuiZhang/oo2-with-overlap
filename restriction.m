function R = restriction(n,bounds)
%restriction -- compute the restriction matrix for subdomain
%
%   R = restriction(n,bounds)
%
%   INPUTS
%
%   n -- n(1),n(2) number of elements in x and y dimensions
%
%   bounds -- (ix1,ix2,iy1,iy2)^T, 2D subscripts beginning and ending
%   that define the subdomain elements
%
%   OUTPUTS
%
%   R -- the sparse matrix that mapps the domain vector to the subdomain
%   vector, including the subdomain boundary


%   compute the [i,j,v] of R
% jj is global indices
ix= bounds(1):bounds(2)+1;
iy= bounds(3):bounds(4)+1;
[Ix Iy]= meshgrid(ix,iy);
jj= (Ix(:)-1)*(n(2)+1) + Iy(:);
% ii is local indices
nlocal= [bounds(2)-bounds(1)+1, bounds(4)-bounds(3)+1];
[Ix Iy]= meshgrid(1:nlocal(1)+1,1:nlocal(2)+1);
ii= (Ix(:)-1)*(nlocal(2)+1) + Iy(:);
% vv is always one
ni= (nlocal(1)+1)*(nlocal(2)+1);
vv= ones(ni,1);

%   construction of sparse matrix R
R= sparse(ii,jj,vv,ni,(n(2)+1)*(n(1)+1));