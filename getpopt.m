function pp = getpopt(w,optctx,m)
%getpopt -- compute optimized coefficients for the tangential operator
%
%   pp = getpopt(w,optctx)
%
%   w -- wavenumber for the PDE - rho * div (1/rho grad u) + w^2 u = S
%        but may also contain rho in its second column
%
%   optctx -- {sgn,dwp,dwm,CL,r,h,form,rir,H,N,o}
%
%     sgn -- sign convention
%
%     dwp,dwm -- k in [w-dwm, w+dwp] are excluded for optimization
%
%     L -- overlap size (along normal direction)
%
%     r -- h ~ w^r
%
%     h -- mesh size along tangential direction
%
%     rir -- ratio of real part to imaginary part
%
%     form -- 'O2',
%
%     H -- subdomain size along x,y directions
%
%     N -- number of subdomains along x,y directions
%
%     o -- number of elements to extend in x,y directions
%
%     hall -- mesh sizes in x,y dimensions
%
%     kmin, kmax -- min and max spatial frequencies
%
%     agm -- for two-level, aggregate agm(w,h) fine element to one coarse
%     element
%
%  m -- 1 for left side, 2 for right, 3 for bottom, 4 for top

%   initialization
sgn= optctx{1}; dwp= optctx{2}; dwm= optctx{3}; L= optctx{4};
r= optctx{5}; h= optctx{6}; rir= optctx{7}; form= optctx{8};
kmin= optctx{13}; kmax= optctx{14}; agm= optctx{15}; 
aref = optctx{16}; wref = optctx{17};
Ch= h*w^r;
persistent called;
if isempty(called)
    called= 0;
end
if iscell(form)
    if nargin>2
        form= form{m};
    else
        form= form{1};
    end
end

%   compute pp
switch form
  case 'O0a'                            % Gander 2002 non-overlapping
    w= w(1);
    pp(1)= ((kmax^2-w^2)*(w^2-(w-dwm)^2))^.25/2^.5*(rir+1i*sgn);
    pp(2)= 0;
  case 'O0'                             % FIXME: valid for r=1 only
    w= w(1);
    if dwp>=dwm
        Cq= (2*dwm)^.25*(dwp/(dwp+dwm))^.5;
        Cp= Cq*sqrt(dwm/dwp);
    else
        Cp= (dwp/2)^(1/4);
        Cq= Cp;
    end
    pp(1)= Cp*w^(3/4)*rir+sgn*1i*Cq*w^(3/4);
    pp(2)= 0;
  case 'T2'
    w= w(1);
    if kmin<w || 1
        %fprintf('kmin<w\n');
        k0= 0;
        pp(1)= sgn*(1i)*sqrt(w^2-k0^2)+sgn*1i*k0^2*0.5/sqrt(w^2-k0^2);
        pp(2)= -sgn*0.5i/sqrt(w^2-k0^2);
        pp(3)= sgn*1.5i*sqrt(w^2-k0^2);
    else
        %fprintf('kmin>w\n');
        pp(1)= 10;
        pp(2)= 0;%-0.5i/w;
    end
  case 'T0'
    pp(1)= sgn*1i*w(1);
    pp(2)= 0;
    pp(3)= 0;
  case 'fine'
    pp(1)= .5i*w(1);
    pp(2)= -0.0i/w(1);
  case 'coarse'
    pp(1)= 0.0999i*w(1);
    pp(2)= -0.0i/w(1);
  case 'O2',
    if ~isempty(aref) && ~isempty(wref)
        w= wref/sqrt(aref);
    else
        w= w(1);
    end
    if abs(imag(w^2))>1e-6
        % formula Bouajaji-Dolean-Gander-Lanteri 2012, Algorithm 4
        % where L=h, and w is fixed!
        beta= abs(imag(w^2));
        pa= beta^.2/2/h^.6;
        pb= beta^.4/2/h^.2;
        pa= pa*(1+sgn*1i); pb= pb*(1+sgn*1i);
        pp(1)= (pa*pb-w^2)/(pa+pb);  pp(2)= 1./(pa+pb);
    else
        LL= L/2;
        CL= LL/h;
        dw= min(dwp,dwm);
        Cw= min(dwp^2+2*dwp*w,2*dwm*w-dwm^2);
        if 1<=r && r<9/8
            pa= dw^(3./8.)*(w/2)^(5./8.);
            pb= (2*dw)^(1./8.)*w^(7./8.);
        elseif r>9/8
            pa= (dw*w)^(2./5.)*LL^(-1./5.)/2.;
            pb= (dw*w)^(1./5.)*LL^(-3./5.)/2.;
        elseif 9/8==r
            pa= (Ch*CL*dw)^(1./3.)*w^(5./8.);
            pb= Ch*CL*w^(7./8.);
        elseif 0==r
            pa= Cw^(2./5.)*(4*LL)^(-1./5.)/2;
            pb= Cw^(1./5.)*(4*LL)^(-3./5.);
        else
            error('wrong arg','getpopt: wrong r, must be >=1 or be zero');
        end
        pa= (rir+sgn*1i)*pa;  pb= (rir+sgn*1i)*pb;
        pp(1)= (pa*pb-w^2)/(pa+pb);  pp(2)= 1./(pa+pb);
    end
  case 'O2a',                           % non-overlapping, Gander 2002
    w= w(1);
    pa= sgn*1i*((w^2-kmin^2)*(w^2-(w-dwm)^2))^.25;
    pb= ((kmax^2-w^2)*((w+dwp)^2-w^2))^.25;
    pp(1)= (pa*pb-w^2)/(pa+pb);  pp(2)= 1./(pa+pb);
  case 'O2b',                           % non-overlapping, Gander 2007
    w= w(1);
    if 1==r
        dw= min(dwp,dwm);
        C= kmax*h;
        pa= (2*dw)^.125 *(C^2-Ch^2)^(3./8.) /Ch^.75 *w^(7./8.);
        pb= (2*dw)^(3./8.) *(C^2-Ch^2)^.125 /Ch^.25 /2 *w^(5./8.);
    elseif r>1
        pa= (2*dw)^.125 *(C/Ch)^.75 *w^((6*r+1)/8.);
        pb= (2*dw)^(3./8.) *(C/Ch)^.25 /2 *w^((2*r+3)/8.);
    elseif 0==r
        pa= C^.75*Cw^.125 /h^.75;
        pb= C^.25*Cw^(3./8.)/h^.25/2;
    else
        error('wrong arg','getpopt: wrong r, must be >=1 or be zero');
    end
    pa= (rir+sgn*1i)*pa; pb= (rir+sgn*1i)*pb;
    pp(1)= (pa*pb-w^2)/(pa+pb);  pp(2)= 1./(pa+pb);
  case 'O2z',                           % Zolotarjov's one-sided
    w = w(1);
    s = zolnew(1,sqrt(w^2-(w-kmin)^2),sqrt(w^2-kmin^2))*1i*sgn;
    pp(1) = (s(1)*s(2)-w^2)/(s(1)+s(2));
    pp(2) = 1/(s(1)+s(2));
  case 'Penalty',
    pp(1)= 1e12/h; pp(2)= 0;
  case 'O1s2',                          % use two-sided parameters of zero-order
                                        % from Erwin's thesis
    rho = w(3)/w(4);
    if rho>1
        swap = true;
        rho = 1/rho;
        w1 = w(2);
        w2 = w(1);
    else
        swap = false;
        w1 = w(1);
        w2 = w(2);
    end
    cw1 = w1*h; cw2 = w2*h; cr = sqrt(abs(cw1^2-cw2^2));
    cm1 = sqrt(pi^2-cw1^2); cm2 = sqrt(pi^2-cw2^2);
    F = @(p1,p2) [p1^2*(2*p2^2-2*p2*cr+cr^2)/p2^2/(2*p1^2+2*p1*cr*rho+cr^2*rho^2)-...
                  rho^2*p2^2*(2*p1^2-2*p1*cr+cr^2)/p1^2/(2*p2^2*rho^2+2*p2*cr*rho+cr^2), ...
                  p1^2*(2*p2^2-2*p2*cr+cr^2)/p2^2/(2*p1^2+2*p1*cr*rho+cr^2*rho^2)-...
                  rho^2*(2*p2^2-2*p2*cm2+cm2^2)*(2*p1^2-2*p1*cm1+cm1^2)/...
                  (2*p2^2*rho^2+2*p2*cm2*rho+cm2^2)/...
                  (2*p1^2+2*p1*cm1*rho+cm1^2*rho^2)];
    G = @(p) F(p(1),p(2));
    s = fsolve(G,[1,1],optimoptions('fsolve','Display','off','MaxIter',1000));
    fac = @(p1,p2) (p1^2*(2*p2^2-2*p2*cr+cr^2)/p2^2/(2*p1^2+2*p1*cr*rho+cr^2*rho^2))^(1/4);
    factor = fac(s(1),s(2));
    if swap
        pp(1) = s(1)/h*(1+1i)/w(3);
    else
        pp(1) = s(2)/h*(1+1i)/w(4);
    end
    pp(2) = 0;
  case 'O2s2',                          % two-sided parameters of second-order
%    if w(1)==w(2) && w(3)==w(4)
        
    if ~isempty(aref) && ~isempty(wref)
        w= wref/sqrt(aref);
    else
        w= w(1);
    end
        % Zolotarjov propagating & evanescent
% $$$         s = zolnew(1,sqrt(w^2-(w-dwm)^2),sqrt(w^2-kmin^2))*1i*sgn;
% $$$         zhigh = 1.5;
% $$$         s = [s;zolnew(1,sqrt((w+dwp)^2-w^2),sqrt(zhigh*w^2-w^2))];
        % Zolotarjov propagating only
        s = zolnew(2,sqrt(w^2-(w-dwm)^2),sqrt(w^2-kmin^2))*1i*sgn;
        if m==1 || m==3
            pp(1) = (s(1)*s(2)-w^2)/(s(1)+s(2));
            pp(2) = 1/(s(1)+s(2));
        else
            pp(1) = (s(3)*s(4)-w^2)/(s(3)+s(4));
            pp(2) = 1/(s(3)+s(4));
        end
%    else                                % to be researched
%        disp('error here');
%    end
  otherwise,
    disp('getpopt: unknown optctx{8}');
end

if ~called
    format long, pp, format
end
called= called+1;