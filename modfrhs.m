function bs = modfrhs(b,Ro,Rp,obounds,n,numpml,h,rhsctx)
%modfrhs -- modify rhs for subdomains
%
%  bs = modfrhs(b,Ro,obounds,n,h,rhsctx)
%
%  Ro -- Ro{s} is from the original(+PML) domain to subdomain+overlap(+DDPML)
%
%  numpml -- a structure containing information of original and DD- PML
%
%  numpml.orig
%
%  numpml.origs
%
%  numpml.dds
%
%  rhsctx -- {domain,scpoints,Sp,sclines,Sl,Sa}, context for re-assembly of
%  rhs at interface nodes
%       
%     domain -- (xl,xr,yl,yr) of the global domain
% 
%     scpoints -- (x,y) of point sources
%
%     Sp -- strength of point sources
% 
%     sclines -- line sources, (x1,y1,x2,y2) beginning and ending points
%
%     Sl -- strength of line sources
%
%     Sa -- area source function

%   initialization
Ns= length(Ro);
bs= cell(Ns,1);

%   special case 1:  no modification 
if nargin==2
    for s= 1:Ns
        bs{s}= Ro{s}*b;
    end
    return;
end

%   special case 2:  half source on subdomain interface (quarter at corners)
if nargin==6    
    for s= 1:Ns
        if ~isempty(Rp) && ~isempty(Rp{s})
            bls= Rp{s}*(Ro{s}*b);
        else
            bls= Ro{s}*b;
        end
        nphy= [obounds(2,s)-obounds(1,s)+1, obounds(4,s)-obounds(3,s)+1];
        nloc= nphy+[sum(numpml.origs(1:2,s)), sum(numpml.origs(3:4,s))];
        if (nloc(1)+1)*(nloc(2)+1)~=size(bls,1)
            error('modfrhs: inconsistent inputs');
        end
        if obounds(1,s)>1
            ib= 1:nloc(2)+1;
            bls(ib)= bls(ib)/2;
        end
        if obounds(2,s)<n(1)
            ib= (1:nloc(2)+1)+nloc(1)*(nloc(2)+1);
            bls(ib)= bls(ib)/2;
        end
        if obounds(3,s)>1
            ib= 1+(0:nloc(1))*(nloc(2)+1);
            bls(ib)= bls(ib)/2;
        end
        if obounds(4,s)<n(2)
            ib= (1:nloc(1)+1)*(nloc(2)+1);
            bls(ib)= bls(ib)/2;
        end
        if ~isempty(Rp) && ~isempty(Rp{s})
            bs{s}= Rp{s}.'*bls;
        else
            bs{s}= bls;
        end
    end
    return;
end

%   extract informations
domain= rhsctx{1}; scpoints=rhsctx{2}; Sp=rhsctx{3}; sclines= rhsctx{4};
Sl= rhsctx{5}; Sa= rhsctx{6};
xl= domain(1,1);  yl= domain(2,1);

%   for every subdomain
for s= 1:Ns
    %   copy of b
    bb= b;
    %   point sources
    [ix,iy]=findscp(n,h,domain,scpoints);
    inodes= [];
    % point sources immediately outside left side
    tf= ix==obounds(1,s)-1 & (iy>=obounds(3,s) & iy<=obounds(4, s));
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)];
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)+1];
    % right side
    tf= (ix==obounds(2,s)+1) & (iy>=obounds(3,s)) & (iy<=obounds(4, s));
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)];
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)+1];
    % bottom side
    tf= iy==obounds(3,s)-1 & (ix>=obounds(1,s) & ix<=obounds(2, s));
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)+1];
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)+1];
    % top side
    tf= iy==obounds(4,s)+1 & (ix>=obounds(1,s) & ix<=obounds(2, s));
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)];
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)];
    % corners
    tf= ix==obounds(1,s)-1 & iy==obounds(3,s)-1;
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)+1];
    tf= ix==obounds(1,s)-1 & iy==obounds(4,s)+1;
    inodes= [inodes;ix(tf)*(n(2)+1)+iy(tf)];
    tf= ix==obounds(2,s)+1 & iy==obounds(3,s)-1;
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)+1];
    tf= ix==obounds(2,s)+1 & iy==obounds(4,s)+1;
    inodes= [inodes;(ix(tf)-1)*(n(2)+1)+iy(tf)];
    inodes= unique(inodes);
    bb(inodes)= 0;
    %   line sources
    if ~isempty(sclines)
        disp('modfrhs - sclines not implemented yet');        
    end
    %   area sources
    
    %   restriction of modified b
    bs{s}= Ro{s}*bb;
end