function [A,R,nall,RD]= assembly(n,h,a,w,bctype,p,tgv,pmlctx,positive,w4pml)
%assembly -- Assemble the matrix for the Helmholtz equation
%
%  [A,R,nall] = assembly(n,h,a,w,bctype,p,tgv,pmlctx) discretize the Helmholtz
%  operator on a uniform cartesian mesh using Q1 finite element method.
%
%  OUTPUT
%
%  A --  assembled matrix
%
%  R --  restriction matrix to the physical domain when pml is used
%
%  nall -- number of elements including possible PML in x and y directions
%
%  RD -- restriction matrix to Dirichlet nodes
%
%  INPUT
%
%  n  -- n(1), n(2) numbers of elements in the two dimensions
%
%  h  -- h(1), h(2) mesh size in the two dimensions
% 
%  a  -- array of density data, each for one element, -div(a grad u)
% 
%  w  -- array of wavenumber, each for one element, - w^2 u
%
%  bctype -- types of boundary conditions on four sides: left, right, bottom
%  and top, 0 Neumann, 1 Robin-like, 2 Dirichlet
%
%  p -- parameters for the tangential operator p0 - p1 D_tt in Robin-like
%  b.c. and in corner condition D_n1 + D_n2 + p2, as function of w
%
%  tgv -- 1 or large number, if 1, zero rows for Dirichlet nodes and put 1
%  on diagonal, otherwise, i.e. large number, put diagonal values as tgv,
%  but do not change non-diagonal values
%
%  pmlctx -- context of the pml/auxiliary system
%
%  positive -- this makes asmvol to assemble the modified Helmholtz with the
%  operator -Lap + w^2
%
%  w4pml -- intended for using different 'w' values (of the same size as w) for
%  pml, e.g. when 'w' in the physical domain is modified but we to use the
%  original 'w' for pml


%   if has pml, the nodes include those in pml
if ~exist('w4pml','var')
    w4pml= [];
end
if ~isempty(find(bctype==4,1)) && exist('pmlctx','var') && ~isempty(pmlctx) && pmlctx.nl>0
    [A,R,nall,RD]= asmpml(n,h,a,w,bctype,tgv,pmlctx,w4pml);
    return;
else
    R= []; nall= n;
end

%   volume assembly (purely Neumann b.c.)
if exist('positive','var') && ~isempty(positive);
    [ii jj vi]= asmvol(n,h,a,w,positive);
else
    [ii jj vi]= asmvol(n,h,a,w);
end

%   boundary assembly for Robin-like b.c.
inode1= [1, n(1)*(n(2)+1)+1, 1, n(2)+1];
dispnode= [1, 1, n(2)+1, n(2)+1];
iele1= [1, (n(1)-1)*n(2)+1, 1, n(2)];  
dispele= [1, 1, n(2), n(2)];
nbe= [n(2), n(2), n(1), n(1)];
hbe= [h(2), h(2), h(1), h(1)];
tnbe= 0; 
ib= zeros(4,2*n(1)+2*n(2)); jb= ib; vb= ib;
for m= 1:4
    if bctype(m)~=1
        continue;
    end
    [ib(:,tnbe+(1:nbe(m))),jb(:,tnbe+(1:nbe(m))),vb(:,tnbe+(1:nbe(m)))]= ...
        asmrobin(nbe(m),hbe(m),iele1(m),dispele(m),inode1(m),dispnode(m),a,w,p);
    tnbe= tnbe + nbe(m);
end
ib= ib(:,1:tnbe); jb= jb(:,1:tnbe); vb= vb(:,1:tnbe);

%   corner assembly for second-order Robin-like b.c.
ic= []; vc= ic;
if bctype(1)==1 && bctype(3)==1
    pp= p(w(1,1));  vc= [vc; pp(2)*pp(3)];
    ic= [ic; 1]; 
end
if bctype(1)==1 && bctype(4)==1
    pp= p(w(n(2),1));  vc= [vc; pp(2)*pp(3)];
    ic= [ic; n(2)+1]; 
end
if bctype(2)==1 && bctype(3)==1
    pp= p(w(1,n(1)));  vc= [vc; pp(2)*pp(3)];
    ic= [ic; n(1)*(n(2)+1)+1];
end
if bctype(2)==1 && bctype(4)==1
    pp= p(w(n(2),n(1)));  vc= [vc; pp(2)*pp(3)];
    ic= [ic; (n(1)+1)*(n(2)+1)];
end

%   construction of the sparse matrix
nn= (n(1)+1)*(n(2)+1);
A= sparse([ii(:);ib(:);ic(:)],[jj(:);jb(:);ic(:)],[vi(:);vb(:);vc(:)],nn,nn);

%   corners: remove off-diagonal entries of Robin's connection to Dirichlet. The
%   initial motivation is that with inhomogeneous Dirichlet the Robin ABC is not
%   working.  Is this useful? No.
% $$$ if bctype(1)==1 && bctype(3)==2
% $$$     A(2,1)= 0;
% $$$ end
% $$$ if bctype(3)==1 && bctype(1)==2
% $$$     A(n(2)+2,1)= 0;
% $$$ end
% $$$ if bctype(1)==1 && bctype(4)==2
% $$$     A(n(2),n(2)+1)= 0;
% $$$ end
% $$$ if bctype(4)==1 && bctype(1)==2
% $$$     A(2*(n(2)+1),n(2)+1)= 0;
% $$$ end
% $$$ if bctype(2)==1 && bctype(3)==2
% $$$     A(n(1)*(n(2)+1)+2,n(1)*(n(2)+1)+1)= 0;
% $$$ end
% $$$ if bctype(3)==1 && bctype(2)==2
% $$$     A((n(1)-1)*(n(2)+1)+1,n(1)*(n(2)+1)+1)= 0;
% $$$ end
% $$$ if bctype(2)==1 && bctype(4)==2
% $$$     A((n(1)+1)*(n(2)+1)-1,(n(1)+1)*(n(2)+1))= 0;
% $$$ end
% $$$ if bctype(4)==1 && bctype(2)==2
% $$$     A(n(1)*(n(2)+1),(n(1)+1)*(n(2)+1))= 0;
% $$$ end


%   Dirichlet b.c.
rows= zeros(2*n(1)+2*n(2)+4,1);  nrows= 0;
for m= 1:4
    if bctype(m)~=2
        continue;
    end
    rows(nrows+1:nrows+nbe(m)+1)= (inode1(m):dispnode(m):inode1(m)+ ...
                                   nbe(m)*dispnode(m))';
    nrows= nrows + nbe(m)+1;
end
rows= rows(1:nrows);
RD= sparse(rows,rows,1,size(A,1),size(A,2));
if tgv==1
    A= setrows0(A,rows,1);
else
    A= setsparse(A,rows,rows,tgv);
end

%   bctype 3: sqrt, same codes as in modify.m
for m=1:4
    if bctype(m)~=3
        continue;
    end
    p0= zeros(nbe(m),1); p2= p0; aa= p0;
    for mm= 1:nbe(m)
        p0(mm)= -w(iele1(m)+(mm-1)*dispele(m))^2;
        p2(mm)= 1;
        aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
    end
    S= asmrobinface(nbe(m),hbe(m),aa,p0,p2); S= full(S); 
    S(1,2:end)= 0; S(2:end,1)= 0; S(end,1:end-1)= 0; S(1:end-1,end)=0;        
    S= sqrtm(S*hbe(m));           % rescale assuming hx=hy
    S(1,:)= 0; S(:,1)=0; S(end,:)= 0; S(:,end)= 0;
    [ib jb vb]= find(S);
    ib= inode1(m)+(ib-1)*dispnode(m);
    jb= inode1(m)+(jb-1)*dispnode(m);
    A= setsparse(A,ib,jb,vb,@plus);
end