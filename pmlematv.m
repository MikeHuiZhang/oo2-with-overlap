function vv = pmlematv(jx,jy,a,w,pmlctx,pmlcty)

%  Inputs Check
% $$$ if jx~=0
% $$$     if ~exist('pmlctx','var') || isempty(pmlctx)
% $$$         error('pmlematv: 1st arg is non-zero but 5th arg pmlctx is empty');
% $$$     elseif ~strcmp(pmlctx.method,'pml')
% $$$         error('pmlematv: is called with pmlctx.method not pml');
% $$$     end
% $$$ end
% $$$ if jy~=0 
% $$$     if (~exist('pmlcty','var') || isempty(pmlcty))
% $$$         error('pmlematv: 2nd arg is non-zero but 6th arg pmlcty is empty');
% $$$     elseif ~strcmp(pmlcty.method,'pml')
% $$$         error('pmlematv: is called with pmlcty.method not pml');
% $$$     end
% $$$ end
% $$$ if jx==0 && jy==0
% $$$     warning('pmlematv: jx and jy are both zeros, return nothing');
% $$$     vv= []; return;
% $$$ end
% $$$ if jx~=0 && jy~=0
% $$$     assert(strcmp(pmlctx.method,pmlcty.method), ['pmlctx.method must be ' ...
% $$$                         'pmlcty.method']);
% $$$ end


%  1-D stencils
if jx~=0
    pmlctx.axi= 1;
    if isfield(pmlctx,'aref') && isfield(pmlctx,'wref')
        [sx,pts,wts]= pmlctx.sx(jx,pmlctx.aref,pmlctx.wref, pmlctx);
    else
        [sx,pts,wts]= pmlctx.sx(jx,a,w,pmlctx);
    end
    if length(sx)==1
        Kx= [1 -1]*sx/pmlctx.h(1); Mx= [1/3 1/6]/sx*pmlctx.h(1); Mx2= Mx;
    else
        Kx= [1 -1]*sum(wts.*sx)/pmlctx.h(1);
        if jx>0
            Mx= [sum(wts.*(1-pts).*(1-pts)./sx), sum(wts.*pts.*(1-pts)./sx)]* ...
                pmlctx.h(1);
            Mx2= [sum(wts.*pts.*pts./sx)*pmlctx.h(1), Mx(2)];
        else
            Mx= [sum(wts.*pts.*pts./sx), sum(wts.*pts.*(1-pts)./sx)]* ...
                pmlctx.h(1);
            Mx2= [sum(wts.*(1-pts).*(1-pts)./sx)*pmlctx.h(1), Mx(2)];
        end
    end
else
    Kx= [1 -1]/pmlcty.h(1); Mx= [1/3 1/6]*pmlcty.h(1); Mx2= Mx;
end
if jy~=0
    pmlcty.axi= 2; 
    if isfield(pmlcty,'aref') && isfield(pmlcty,'wref')
        [sy,pts,wts]= pmlcty.sy(jy,pmlcty.aref,pmlcty.wref, pmlcty);
    else
        [sy,pts,wts]= pmlcty.sy(jy,a,w,pmlcty);
    end
    if length(sy)==1
        Ky= [1 -1]*sy/pmlcty.h(2); My= [1/3 1/6]/sy*pmlcty.h(2); My2= My;
    else
        Ky= [1 -1]*sum(wts.*sy)/pmlcty.h(2);
        if jy>0
            My= [sum(wts.*(1-pts).*(1-pts)./sy), sum(wts.*pts.*(1-pts)./sy)]* ...
                pmlcty.h(2);
            My2= [sum(wts.*pts.*pts./sy)*pmlcty.h(2), My(2)];
        else
            My= [sum(wts.*pts.*pts./sy), sum(wts.*pts.*(1-pts)./sy)]* ...
                pmlcty.h(2);            
            My2= [sum(wts.*(1-pts).*(1-pts)./sy)*pmlcty.h(2), My(2)];
        end
    end
else
    Ky= [1 -1]/pmlctx.h(2); My= [1/3 1/6]*pmlctx.h(2); My2= My;                  
end

%  2-D stencils to values of element matrix
vv= zeros(16,1);
vv(1:4)= a*(Kx.'*My+Mx.'*Ky) - w^2*Mx.'*My;
vv(5:8)= a*(Kx.'*My+Mx2.'*Ky) - w^2*Mx2.'*My;
vv(9:12)= a*(Kx.'*My2+Mx2.'*Ky) - w^2*Mx2.'*My2;
vv(13:16)= a*(Kx.'*My2+Mx.'*Ky) - w^2*Mx.'*My2;