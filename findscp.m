function [ix,iy]=findscp(n,h,domain,scpoints)
    ix= 1+floor((scpoints(:,1)-domain(1,1))/h(1));
    iy= 1+floor((scpoints(:,2)-domain(2,1))/h(2));
    if sum(ix>n(1))
        ix(ix>n(1))= n(1);  scpoints(ix>n(1),1)= domain(1,2);
    end
    if sum(iy>n(2))
        iy(iy>n(2))= n(2);  scpoints(iy>n(2),2)= domain(2,2);
    end    
end
