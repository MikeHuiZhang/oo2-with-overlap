function c = fread2mesh(file,ndata,nmesh,form)

% read binary data
if form=='bin'
    fid= fopen(file,'r','ieee-le');
    data= fread(fid,inf,'float');
    fclose(fid);
else
    % or use this for ASCII/MAT file
    data= load(file);
end

data= reshape(data,[ndata(2),ndata(1)]);
c= zeros(nmesh(2),nmesh(1));
myfun= @(z) z; % 1.0/z
myfuninv= myfun;
for ix= 1:nmesh(1)
    hix= 1/nmesh(1); hjx= 1/ndata(1);
    jxs= ceil((ix-1)*1/nmesh(1)*ndata(1)); jxs= max(1,jxs);
    jxe= ceil(ix*1/nmesh(1)*ndata(1));
    for iy= 1:nmesh(2)
        hiy= 1/nmesh(2); hjy= 1/ndata(2);
        jys= ceil((iy-1)*1/nmesh(2)*ndata(2)); jys= max(1,jys);
        jye= ceil(iy*1/nmesh(2)*ndata(2));
        ii= (ix-1)*nmesh(2)+iy;
        for jx= jxs:jxe
            rx= (min(jx*hjx,ix*hix) - max(jx*hjx-hjx,ix*hix-hix))/hix;
            for jy= jys:jye
                ry= (min(jy*hjy,iy*hiy) - max(jy*hjy-hjy,iy*hiy-hiy))/hiy;
                jj= (jx-1)*ndata(2)+jy;
                c(ii)= c(ii) + myfun(data(jj))*rx*ry;
            end
        end
        c(ii)= myfuninv(c(ii));
    end
end