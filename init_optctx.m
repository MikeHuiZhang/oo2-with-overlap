function optctx= init_optctx(optctx,m)
%init_optctx 
%
%  initialize some entries of optctx
%
%  m -- one of the four sides of a rectangle, 1 left, 2 right, 3 bottom, 4
%  top
%
%
%  c.f. getpopt.m for structure of optctx

H= optctx{9}; N= optctx{10}; o= optctx{11}; h= optctx{12};
Hi= [H(2) H(2) H(1) H(1)];
hi= [h(2) h(2) h(1) h(1)];
hn= [h(1) h(1) h(2) h(2)];
Nn= [N(1) N(1) N(2) N(2)];
Ni= [N(2) N(2) N(1) N(1)];
oo= [o(1) o(1) o(2) o(2)];
if isempty(optctx{2})                   % dwp
    optctx{2}= pi/Hi(m);
end
optctx{3}= optctx{2};                   % dwm: could use a different value
optctx{4}= 2*oo(m)*hn(m);
optctx{6}= hi(m);
optctx{7}= 0/Nn(m);                     % real/imag: 1, 1/Nn(m)
optctx{13}= pi/Hi(m);
optctx{14}= pi/hi(m);