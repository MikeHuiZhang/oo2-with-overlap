function [ii,jj,vi] = asmvol(n,h,a,w,positive)
%asmin -- assembly in volume for the Helmholtz equation.
%
%  [ii jj vi] = asmvol(n,h,a,w) computes the [i j v] arrays for the sparse
%  matrix corresponding to the Helmholtz equation with purely Neumann b.c.
%
%  OUTPUTS
%
%  ii,jj,vi -- 16-by-n arrays for the sparse matrix
%
%  INPUTS
%
%  n -- n(1), n(2) are numbers of elements in the two dimensions
%
%  h -- mesh sizes in the two dimensions
%
%  a -- array of density data, each for one element, index running first in
%  the second dimension
%
%  w -- array of wavenumber, each for one element, index running first in
%  the second dimension
%
%  positive -- this changes the operator to the positive -Lap + w^2

%   element stiffness and mass stencils
%     2  3
%     | /   
%     0--1
hxy= h(1)/h(2);  hyx= 1/hxy;
Ks= [hxy+hyx, 0.5*hxy-hyx, 0.5*hyx-hxy, -0.5*(hxy+hyx)]/3.0;
Ms= [1/9.0, 1/18.0, 1/18.0, 1/36.0]*h(1)*h(2);


%   for every element, compute [i j v]
%     4--3
%     |  |
%     1--2
%   ii(1:4,iele) is for node 1 whose global number is inode1
ii= zeros(16,n(1)*n(2)); jj= ii; vi= ii;
I= [1:4, 1:4, 1:4, 1:4];
if exist('positive','var') && positive
    sgn= -1;
else
    sgn= 1;
end
for ix= 1:n(1)
    for iy= 1:n(2)
        iele= (ix-1)*n(2)+iy;  inode1= (ix-1)*(n(2)+1)+iy;
        ii(1:4,iele)= inode1;
        jj(1:4,iele)= inode1+[0; n(2)+1; 1; n(2)+2];
        ii(5:8,iele)= inode1+n(2)+1;
        jj(5:8,iele)= inode1+[n(2)+1; 0; n(2)+2; 1];
        ii(9:12,iele)= inode1+n(2)+2;
        jj(9:12,iele)= inode1+[n(2)+2;1;n(2)+1;0];
        ii(13:16,iele)= inode1+1;
        jj(13:16,iele)= inode1+[1;n(2)+2;0;n(2)+1];
        vi(1:16,iele)= a(iy,ix)*Ks(I)- sgn*w(iy,ix)^2*Ms(I);
    end
end