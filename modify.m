function [Rom,A,Rp,Aneum,Aaug,mpml,mdpml] = modify(Ro,A,n,bounds,matctx,optctx,sub,N,nphy,pbounds,Ag)
%modify -- modify subdomain matrix
%
%   [Rom,A] = modify(Ro,A,n,bounds,matctx,optctx,sub,N) can be used when the
%   original domain and subdomain are without PML
%
%   [Rom,A,Rp] = modify(..) is for the case with PML on subdomain but not on the
%   original domain, Rp-- restriction matrix to non-ddpml nodes
%
%   [Rom,A,Rp] = modify(Ro,A,n,bounds,matctx,optctx,sub,N,nphy,pbounds) can be
%   used with/without PML on the original domain and subdomain
%
%   [Rom,A,Rp,Aneum,Aaug,mpml,mdpml] = modify(..) outputs also Aneum-- the same
%   size as input A and is subdomain(excluding DD-PML)-to-Neumann trace (on this
%   subdomain's boundary) matrix, Aaug-- the same size as output A but contains
%   only tangential matrix for DD-ABC or DD-PML matrix without contribution from
%   non-ddpml elements, mpml/mdpml-- number of original/DD PML elements normal
%   to four sides of the subdomain.  Note that the output 'A' does not contain
%   complete information about Aneum and Aaug because it is assembled with
%   ABC/PML.
%
%   [..] = modify(..,Ag) computes exterior Schur complement from the original
%   matrix Ag onto (after dropping small than threshold entries) the
%   subdomain matrix A
%
%   OUTPUTS
% 
%   Rom -- for Dirichlet transmission in residual form, we needs to zero out
%   the residual on the Dirichlet boundary, so Ro is modified to Rom
%
%   INPUTS
%
%   Ro -- restriction matrix from orignal domain (with PML) to subdomain
%   (without DD-PML)
%
%   A -- subdomain matrix
%
%   n -- n(1),n(2) number of elements in x,y of the global pml-augmented domain
%
%   bounds -- [x1,x2,y1,y2] beginning and ending elements in x,y, global
%   indexed
%
%   matctx -- {a,w,p,tgv,h,bctype,popt,stretch,popt2,pmlctx,R,nall,pmlddctx}
%
%     a -- density array
%
%     w -- wavenumber array, constant for each element of the global domain
%
%     p -- exterior Robin-like parameters, [p1,p2,p3], as function of w
%
%     tgv -- 1 or large number, if 1, first zero rows for Dirichlet nodes
%     then put 1 on diagonal, if large number, only put tgv on diagonal
%
%     h -- h(1),h(2) mesh size in x and y
%
%     bctype -- e.g. [1 1 2 0] for Robin on left and right, Dirichlet on bottom,
%     etc. of the global domain
%
%     popt -- interface Robin-like parameters, as function of w and optctx
%
%     stretch -- c.f. strchp.m rescale popt for different subdomains
%
%     popt2 -- used for two-level Robin parameters
%
%     pmlctx -- pml context for original and subdomain problems
%
%     R -- matrix of restriction to physical nodes in the original PML augmented
%     domain
%
%     nall -- number of elements in x,y of the global pml-augmented domain
%
%     pmlddctx -- pml context for domain decomposition
%
%   optctx -- c.f. getpopt.m for structure of optctx
%
%   sub -- [sx, sy], 2D location of the subdomain
%
%   N -- [Nx, Ny], number of subdomains in x and y directions
%
%   nphy -- number of elements in the original physical domain
%
%   pbounds -- inclusive bounds of subdomain elements in the original physical
%
%   Ag -- global matrix


%   initialization
Rp= []; mpml= zeros(4,1); npml= zeros(4,2); mdpml= mpml;
a= matctx{1}; w= matctx{2}; p= matctx{3}; tgv= matctx{4};
h= matctx{5}; bctype= matctx{6}; popt=matctx{7}; stretch= matctx{8};
nloc= [bounds(2)-bounds(1)+1, bounds(4)-bounds(3)+1];
if length(matctx)>11 && ~isempty(find(bctype==4,1))
    if isempty(matctx{10})
        error('modify: matctx{10} is empty while bctpye has type 4');
    end
    if ~exist('nphy','var') || ~exist('pbounds','var')
        error('modify: input arg nphy or pbounds are missing');
    end
    pmlctx= matctx{10}; R= matctx{11};
    pmlctx.h = h; pmlctx.H = n(:).*h(:);
    nphyloc= [pbounds(2)-pbounds(1)+1, pbounds(4)-pbounds(3)+1];
else
    pmlctx= []; R= []; nphyloc= nloc;
end
inode1= [1, nloc(1)*(nloc(2)+1)+1, 1, nloc(2)+1];
dispnode= [1, 1, nloc(2)+1, nloc(2)+1];
nbe= [nloc(2), nloc(2), nloc(1), nloc(1)];
htn= [h(2), h(2), h(1), h(1); h(1), h(1), h(2), h(2)];
tnbe= 0;
ib= zeros(4,2*nloc(1)+2*nloc(2)); jb= ib; vb= ib;
ictype= 1*[bounds(1)>1,bounds(2)<n(1),bounds(3)>1,bounds(4)<n(2)]- 1;
ictx= optctx{8};
if ~iscell(ictx)
    ictx= {ictx,ictx,ictx,ictx};
end
switch length(ictx)
  case 0
    error('modify: optctx{8} is empty!');
  case 1
    ictx= {ictx{1},ictx{2},ictx{3},ictx{4}};
  case {2,3}
    ictx= {ictx{1},ictx{2},ictx{1},ictx{2}};
    if length(ictx)==3
        warning('modify: optctx{8} has 3, only the 1st 2 are used.');
    end
end
for m= 1:4
    switch ictx{m}
      case 'PML'
        ictype(m)= (ictype(m)+1)*5-1;
      case 'Neumann'
        ictype(m)= ictype(m);
      case 'Dirichlet'
        ictype(m)= (ictype(m)+1)*3-1;
      case 'sqrt'
        ictype(m)= (ictype(m)+1)*4-1;
      otherwise
        ictype(m)= (ictype(m)+1)*2-1;
    end
end
if ~isempty(pmlctx) && ~isempty(find(bctype==4,1))
    % number of original pml normal to fours sides of subdomain
    mpml= (bctype==4).*(ictype<0)*pmlctx.nl;
    % numbers of original pml along four sides of subdomain, two parts
    npml= [mpml(3) mpml(4);mpml(3) mpml(4);
           mpml(1) mpml(2);mpml(1) mpml(2)];
    % for indexing a, w along four sides
    iele1= [(pbounds(1)-1)*nphy(2)+pbounds(3), ...
            (pbounds(2)-1)*nphy(2)+pbounds(3), ...
            (pbounds(1)-1)*nphy(2)+pbounds(3), ...
            (pbounds(1)-1)*nphy(2)+pbounds(4)];
    dispele= [1, 1, nphy(2), nphy(2)];
    dispw= [-nphy(2), nphy(2), -1, 1];
else
    iele1= [(bounds(1)-1)*n(2)+bounds(3), (bounds(2)-1)*n(2)+bounds(3), ...
            (bounds(1)-1)*n(2)+bounds(3), (bounds(1)-1)*n(2)+bounds(4)];
    dispele= [1, 1, n(2), n(2)];
    dispw= [-n(2), n(2), -1, 1];
end


%   Rp: restriction to non-ddpml nodes for PML transmission.
if ~isempty(find(ictype==4,1))
    if length(matctx)>12
        pmlddctx= matctx{13};
        if ~isempty(pmlctx)
            assert(strcmp(pmlddctx.method,pmlctx.method), ...
                   'original and dd pml must use the same method');
        end
    elseif ~isempty(pmlctx)
        pmlddctx= pmlctx;
    else
        error(['please provide at least either pmlctx in matctx{10} or pmlddctx ' ...
               'in matctx{13}'])
    end
    pmlddctx.H= h(:).*nphyloc(:); pmlddctx.h= h;
    plbounds= [1 nloc(1) 1 nloc(2)];    % in subdomain, bounds of original(including
                                        % PML) elements
    plbounds(1:2)= plbounds(1:2) + (ictype(1)==4)*pmlddctx.nl;
    plbounds(3:4)= plbounds(3:4) + (ictype(3)==4)*pmlddctx.nl;
    nloca= nloc + [(ictype(1)==4)+(ictype(2)==4), ...
                   (ictype(3)==4)+(ictype(4)==4)]*pmlddctx.nl;
    Rp= restriction(nloca,plbounds);
    % number of dd pml normal to four sides of subdomain
    mdpml= (ictype==4)*pmlddctx.nl;
    % number of dd pml along four sides of subdomain, in the beginning & end
    ndpml= [mdpml(3) mdpml(4); mdpml(3) mdpml(4);
            mdpml(1) mdpml(2); mdpml(1) mdpml(2)];
    % truly physical elements in subdomain, inclusive bounds
    phys= [mpml(1)+mdpml(1)+1 nloca(1)-mpml(2)-mdpml(2) ...
           mpml(3)+mdpml(3)+1 nloca(2)-mpml(4)-mdpml(4)];
else
    nloca= nloc;
    plbounds= [1 nloc(1) 1 nloc(2)];
end

%   Schur complements
%TODO: when the original PML is present
%TODO: return also Aneum and Aaug
persistent numsub;
if ~isempty(strfind(cell2mat(ictx),'Schur'))
    warning('Schur found in optctx and will be used for all interfaces');
    if isempty(numsub)
        numsub= 0;
    else
        numsub= numsub+1;
    end
    %  extract the submatrices for the complements of the subdomain
    ixs= bounds(1):1:bounds(2)+1; iys= bounds(3):1:bounds(4)+1;
    [ixs iys]= meshgrid(ixs,iys); iis= (ixs-1)*(n(2)+1)+iys;
    [ixc iyc]= meshgrid(1:n(1)+1,1:n(2)+1); iic= (ixc-1)*(n(2)+1)+iyc;
    iic=iic(:); iis= iis(:); iic= setdiff(iic,iis);
    [Iis Iic]= ndgrid(iis, iic); Asc= getsparse(Ag,Iis,Iic); %size(Asc)
    [Iic Iis]= ndgrid(iic, iis); Acs= getsparse(Ag,Iic,Iis); %size(Acs)
    [Iic Jic]= ndgrid(iic, iic); Acc= getsparse(Ag,Iic,Jic); %size(Acc)
    %  compute the Schur complement and add to the subdomain matrix A
    disp(['Schur complement for subdomain ',num2str(numsub+1),' ...']);
    B= Asc*(Acc\Acs); [ii,jj,vv]= find(B); bmax= max(abs(vv));
    idx= abs(vv)>0.1*bmax;
    disp(['dropping entries less than threshold: nnz before ', ...
          num2str(nnz(B))]);
    B= sparse(ii(idx),jj(idx),vv(idx),size(B,1),size(B,2));
    disp([' and after ', num2str(nnz(B))]);
    A= A - B;
    return;
end

%   assembly for Neumann, this zeros out old self-interaction at corner of
%   interface and exterior Robin or Dirichlet b.c., to be recovered later
%disp('original subdomain matrix'); A
for m= 1:4
    if ictype(m)<0 || ictype(m)==2
        continue;
    end
    if ~isempty(pmlctx)
        pmlctx.axi = 2-(m>2)*1;         % stretching axis of PML along boundary
    end
    [ib,jb,vb]= asmneum(nbe(m),htn(:,m),iele1(m), dispele(m),inode1(m), ...
                        dispnode(m),w,a,pmlctx,npml(m,:));
    A= setsparse(A,ib,jb,0);
    A= setsparse(A,ib,jb,vb,@plus);
end
%disp('Neumman applied'); A


%   recover exterior Robin-like b.c. at corner with interface, we need only
%   to add the self-interaction (in subdomain) corresponding to exterior b.c
%   BEFORE adding DD PML.
if ictype(1)>-.5 && ictype(1)~=2 && bctype(3)==1 && bounds(3)==1
    [ib,jb,vb]= asmrobin(0,h(1),iele1(3),0,inode1(3),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if bctype(1)==1 && ictype(3)>-.5 && ictype(3)~=2 && bounds(1)==1
    [ib,jb,vb]= asmrobin(0,h(2),iele1(3),0,inode1(3),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if ictype(1)>-.5 && ictype(1)~=2 && bctype(4)==1 && bounds(4)==n(2)
    [ib,jb,vb]= asmrobin(0,h(1),iele1(4),0,inode1(4),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if bctype(1)==1 && ictype(4)>-.5 && ictype(4)~=2 && bounds(1)==1
    [ib,jb,vb]= asmrobin(0,h(2),iele1(4),0,inode1(4),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if ictype(2)>-.5 && ictype(2)~=2 && bctype(3)==1 && bounds(3)==1
    [ib,jb,vb]= asmrobin(0,h(1),iele1(2),0,inode1(2),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if bctype(2)==1 && ictype(3)>-.5 && ictype(3)~=2 && bounds(2)==n(1)
    [ib,jb,vb]= asmrobin(0,h(2),iele1(2),0,inode1(2),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if ictype(2)>-.5 && ictype(2)~=2 && bctype(4)==1 && bounds(4)==n(2)
    [ib,jb,vb]= asmrobin(0,h(1),iele1(2)+nphyloc(2)-1,0,...
                             inode1(2)+nloc(2),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
if bctype(2)==1 && ictype(4)>-.5 && ictype(4)~=2 && bounds(2)==n(1)
    [ib,jb,vb]= asmrobin(0,h(2),iele1(2)+nphyloc(2)-1,0,...
                         inode1(2)+nloc(2),0,a,w,p);
    A= setsparse(A,ib,jb,vb,@plus);
end
%disp('corner Robin recovered'); A

%   compute Aneum right after A with Neumann is done, BEFORE adding ABC/PML
[ii jj vv]= find(A);
idxbdry= [1:nloc(2)+1, nloc(1)*(nloc(2)+1)+(1:nloc(2)+1), ...
          1+(0:nloc(1))*(nloc(2)+1), nloc(2)+1+(0:nloc(1))*(nloc(2)+1)];
tf= ismember(ii,idxbdry);
Aneum= sparse(ii(tf),jj(tf),vv(tf),size(A,1),size(A,2));
clear ii jj vv idxbdry tf;


%   assembly for Robin, after Neumann, to avoid to be zeroed by Neumann at
%   corner of two interfaces
%TODO: if neighboring sides of a Robin side use PML, then the Robin side
%can extend into the neighboring PML layers
Aaug= sparse(size(A,1),size(A,2));
if ~isempty(strfind(cell2mat(ictx),'hierarchy'))
    agm= optctx{15};
    for m= 1:4
       if ictype(m)<0 || ~strcmp(ictx{m},'hierarchy') || ictype(m)==2
           continue;
       end
       % compute nodes at coarse level relative to this edge
       nh= agm(w(iele1(m)),htn(1,m));
       icoarse= 1:nh:nbe(m)+1; icoarse(end)= nbe(m)+1;
       % build interpolation matrix R^T from coarse nodal values to fine
       % nodal values
       RHt= interpmat(nbe(m),icoarse);
       % compute Robin matrix with coarse Robin parameter at __fine__ level
       optc= getoptc(optctx,m,nh); p0= zeros(nbe(m),1); p2= p0; aa= p0;
       for mm= 1:nbe(m)
           pp= popt(w(iele1(m)+(mm-1)*dispele(m)),optc);
           p0(mm)= pp(1); p2(mm)= pp(2);
           aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
       end
       Sh= asmrobinface(nbe(m),htn(1,m),aa,p0,p2);
       % restrict the Robin matrix with coarse Robin parameter to coarse level
       SH= RHt'*Sh*RHt; SH= full(SH);
       % Within each coarse element, compute Robin matrix with Robin parameter for
       % subdomain being one coarse element. First compute it as one big matrix
       % for the whole interface then zero out values at coarse nodes
       optf= getoptf(optctx,m,nh); p0= zeros(nbe(m),1); p2= p0; aa= p0;
       popt2= matctx{9};
       for mm= 1:nbe(m)
           pp= popt2(w(iele1(m)+(mm-1)*dispele(m)),optf);
           p0(mm)= pp(1); p2(mm)= pp(2);
           aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
       end
       Sh= asmrobinface(nbe(m),htn(1,m),aa,p0,p2); Sh= full(Sh);
       Sh(icoarse,:)= 0; Sh(:,icoarse)= 0;
       % compute the transform matrix original basis = hierarchy basis * T
       TH= full(RHt); TH(icoarse,:)= -TH(icoarse,:); TH= -TH;
       T= eye(nbe(m)+1); T(:,icoarse)= TH;
       % compute the combined matrix and do the transform
       Sh(icoarse,icoarse)= SH;
       S= T'*Sh*T;
       %S= 1*RHt*SH*(RHt') + 0*Sh;
       % add to A
       [ib jb vb]= find(S);
       ib= inode1(m)+(ib-1)*dispnode(m);
       jb= inode1(m)+(jb-1)*dispnode(m);
       A= setsparse(A,ib,jb,vb,@plus);
       Aaug= setsparse(Aaug,ib,jb,vb,@plus);
    end
elseif ~isempty(strfind(cell2mat(ictx),'two-level'))
    agm= optctx{15};
    for m= 1:4
       if ictype(m)<0 || ~strcmp(ictx{m},'two-level')  || ictype(m)==2
           continue;
       end
       % compute nodes at coarse level relative to this edge
       nh= agm(w(iele1(m)),htn(1,m));
       icoarse= 1:nh:nbe(m)+1; icoarse(end)= nbe(m)+1;
       % build linear interpolation matrix R^T from coarse nodal values to fine nodal
       % values
       RHt= interpmat(nbe(m),icoarse);
       % build 0-1 restriction matrix from fine mesh to coarse mesh
       RH= eye(nbe(m)+1); RH= RH(icoarse,:); RH= sparse(RH);
       % compute Robin matrix with coarse Robin parameter at __fine__ level
       optc= getoptc(optctx,m,nh); p0= zeros(nbe(m),1); p2= p0; aa= p0;
       for mm= 1:nbe(m)
           pp= popt(w(iele1(m)+(mm-1)*dispele(m)),optc);
           p0(mm)= pp(1); p2(mm)= pp(2);
           aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
       end
       Sh= asmrobinface(nbe(m),htn(1,m),aa,p0,p2);
       % restrict the Robin matrix with coarse Robin parameter to coarse level
       SH= RHt'*Sh*RHt; SH= full(SH);
       % compute Robin matrix with Robin parameter for fine mesh
       optf= getoptf(optctx,m,nh);
       p0= zeros(nbe(m),1); p2= p0; aa= p0;
       popt2= matctx{9};
       for mm= 1:nbe(m)
           pp= popt2(w(iele1(m)+(mm-1)*dispele(m)),optf);
           p0(mm)= pp(1); p2(mm)= pp(2);
           aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
       end
       Sh= asmrobinface(nbe(m),htn(1,m),aa,p0,p2); Sh= full(Sh);
       % compute the combined matrix
       S= RHt*SH*RH + Sh;
       % add to A
       [ib jb vb]= find(S);
       ib= inode1(m)+(ib-1)*dispnode(m);
       jb= inode1(m)+(jb-1)*dispnode(m);
       A= setsparse(A,ib,jb,vb,@plus);
       Aaug= setsparse(Aaug,ib,jb,vb,@plus);
    end
elseif ~isempty(strfind(cell2mat(ictx),'sqrt'))% interface square root
    for m= 1:4
        if ictype(m)<0 || ~strcmp(ictx{m},'sqrt')  || ictype(m)==2
            continue;
        end
        p0= zeros(nbe(m),1); p2= p0; aa= p0;
        for mm= 1:nbe(m)
            p0(mm)= -w(iele1(m)+(mm-1)*dispele(m))^2;
            p2(mm)= 1;
            aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
        end
        S= asmrobinface(nbe(m),htn(1,m),aa,p0,p2); S= full(S);
        S(1,2:end)= 0; S(2:end,1)= 0; S(end,1:end-1)= 0; S(1:end-1,end)=0;
        S= sqrtm(S*htn(1,m));           % rescale assuming hx=hy
        S(1,:)= 0; S(:,1)=0; S(end,:)= 0; S(:,end)= 0;
        [ib jb vb]= find(S);
        ib= inode1(m)+(ib-1)*dispnode(m);
        jb= inode1(m)+(jb-1)*dispnode(m);
        A= setsparse(A,ib,jb,vb,@plus);
        Aaug= setsparse(Aaug,ib,jb,vb,@plus);
    end
elseif ~isempty(strfind(cell2mat(ictx),'T4'))% Taylor 4-th order, no improvement
    for m= 1:4
        if ictype(m)<0 || ~strcmp(ictx{m},'T4') || ictype(m)==2
            continue;
        end
        p0= zeros(nbe(m),1); p2= p0; p4= p0; aa= p0;
        for mm= 1:nbe(m)
            p0(mm)= 1i*w(iele1(m)+(mm-1)*dispele(m));
            p2(mm)= -0.5i/w(iele1(m)+(mm-1)*dispele(m));
            p4(mm)= -0.125i/w(iele1(m)+(mm-1)*dispele(m))^3;
            aa(mm)= a(iele1(m)+(mm-1)*dispele(m));
        end
        S= asmrobinface(nbe(m),htn(1,m),aa,p0,p2,p4);
        [ib jb vb]= find(S);
        ib= inode1(m)+(ib-1)*dispnode(m);
        jb= inode1(m)+(jb-1)*dispnode(m);
        A= setsparse(A,ib,jb,vb,@plus);
        Aaug= setsparse(Aaug,ib,jb,vb,@plus);
    end
else                                    % one-level
    for m= 1:4
        if ictype(m)<=0 || ictype(m)>=2
            continue;
        end
        optctx= init_optctx(optctx,m);        
        if stretch
            [ib,jb,vb]= asmrobin(nbe(m),htn(1,m),iele1(m),dispele(m),inode1(m), ...
                                 dispnode(m),a,w, ...
                                 @(w)strchp(m,sub,N)*popt(w,optctx,m),dispw(m));
        else
            [ib,jb,vb]= asmrobin(nbe(m),htn(1,m),iele1(m),dispele(m),inode1(m), ...
                                 dispnode(m),a,w,@(w)popt(w,optctx,m),dispw(m),pmlctx,npml(m,:));
        end
        A= setsparse(A,ib,jb,vb,@plus);
        Aaug= setsparse(Aaug,ib,jb,vb,@plus);
    end
    % corner conditions
    ic = []; vc = [];
    if ictype(1)==1 && ictype(3)==1
        we= w(iele1(1))/sqrt(a(iele1(1)));
        pp= p(we);  vc= [vc; a(iele1(1))*pp(2)*pp(3)];
        ic= [ic; 1];
    end
    if ictype(1)==1 && ictype(4)==1
        we= w(iele1(4))/sqrt(a(iele1(4)));
        pp= p(we);  vc= [vc; a(iele1(4))*pp(2)*pp(3)];
        ic= [ic; nloc(2)+1];
    end
    if ictype(2)==1 && ictype(3)==1
        we= w(iele1(2))/sqrt(a(iele1(2)));
        pp= p(we);  vc= [vc; a(iele1(2))*pp(2)*pp(3)];
        ic= [ic; nloc(1)*(nloc(2)+1)+1];
    end
    if ictype(2)==1 && ictype(4)==1
        iele= iele1(2)-1+nloc(2);
        we= w(iele)/sqrt(a(iele));
        pp= p(we);  vc= [vc; a(iele)*pp(2)*pp(3)];
        ic= [ic; (nloc(1)+1)*(nloc(2)+1)];
    end
    if ~isempty(ic) && ~isempty(vc)
        A= setsparse(A,ic,ic,vc,@plus);
        Aaug= setsparse(Aaug,ic,ic,vc,@plus);
    end
end
%disp('DD Robin applied'); A

%   Dirichlet transmission, modify A and Ro
Etemp = speye(size(A));
for m = 1:4
    if ictype(m)~=2
        continue;
    end
    ib = inode1(m):dispnode(m):inode1(m)+dispnode(m)*nbe(m);
    A = setrows0(A,ib,1);
    % TODO: is the following line useful?
    Aaug = setrows0(Aaug,ib,1);
    %Etemp(ib,ib) = 0;
    Etemp = setsparse(Etemp,ib,ib,0);
end
Rom = Etemp*Ro;
clear Etemp;

%   assembly for DD PML
if ~isempty(find(ictype==4,1))
    A= Rp'*A*Rp; Aaug= Rp'*Aaug*Rp;
    ixs= [1 plbounds(2)+1 1 1];
    ixe= [pmlddctx.nl plbounds(2)+pmlddctx.nl nloca(1) nloca(1)];
    iys= [plbounds(3) plbounds(3) 1           plbounds(4)+1];
    iye= [plbounds(4) plbounds(4) pmlddctx.nl plbounds(4)+pmlddctx.nl];
    for m= 1:4
        if ictype(m)~=4
            continue;
        end
        ii= zeros(16,(ixe(m)-ixs(m)+1)*(iye(m)-iys(m)+1)); jj= ii; vv= ii;
        count= 0;
        for ix= ixs(m):ixe(m)
            for iy= iys(m):iye(m)
                count= count+1;
                i1= (ix-1)*(nloca(2)+1)+iy;
                ii(1:4,count)= i1;
                ii(5:8,count)= i1+nloca(2)+1;
                ii(9:12,count)= i1+nloca(2)+2;
                ii(13:16,count)= i1+1;
                jj(1:4,count)= i1+[0; nloca(2)+1; 1; nloca(2)+2];
                jj(5:8,count)= i1+[nloca(2)+1; 0; nloca(2)+2; 1];
                jj(9:12,count)= i1+[nloca(2)+2; 1; nloca(2)+1; 0];
                jj(13:16,count)= i1+[1;nloca(2)+2;0;nloca(2)+1];
                ixr= min(max(phys(1),ix),phys(2)); iyr= min(max(phys(3),iy),phys(4));
                ixp= ixr-phys(1)+pbounds(1); iyp= iyr-phys(3)+pbounds(3);
                aele= a(iyp,ixp); wele= w(iyp,ixp);
                if m<3
                    if mpml(m)>0
                        pmlctxx= pmlctx;
                    elseif mdpml(m)>0
                        pmlctxx= pmlddctx;
                    else
                        error('modify.m: exceptional case?!');
                    end
                    if iy>=phys(3) && iy<=phys(4)
                        pmlctyy= [];
                    elseif iy<phys(3) && npml(m,1)>0
                        pmlctyy= pmlctx;
                    elseif iy<phys(3) && ndpml(m,1)>0
                        pmlctyy= pmlddctx;
                    elseif iy>phys(4) && npml(m,2)>0
                        pmlctyy= pmlctx;
                    elseif iy>phys(4) && ndpml(m,2)>0
                        pmlctyy= pmlddctx;
                    else
                        error('modify.m: exceptional case?!');
                    end
                else
                    if mpml(m)>0
                        pmlctyy= pmlctx;
                    elseif mdpml(m)>0
                        pmlctyy= pmlddctx;
                    else
                        error('modify.m: exceptional case?!');
                    end
                    if ix>=phys(1) && ix<=phys(2)
                        pmlctxx= [];
                    elseif ix<phys(1) && npml(m,1)>0
                        pmlctxx= pmlctx;
                    elseif ix<phys(1) && ndpml(m,1)>0
                        pmlctxx= pmlddctx;
                    elseif ix>phys(2) && npml(m,2)>0
                        pmlctxx= pmlctx;
                    elseif ix>phys(2) && ndpml(m,2)>0
                        pmlctxx= pmlddctx;
                    else
                        error('modify.m: exceptional case?!');
                    end
                end
                vv(1:16,count)= pmlddctx.ematv(ix-ixr,iy-iyr,aele, wele, ...
                                               pmlctxx,pmlctyy);
            end
        end
        A= setsparse(A,ii,jj,vv,@plus);
        Aaug= setsparse(Aaug,ii,jj,vv,@plus);
    end
    % TODO: Robin BC on the common parts of the original physical boundary and
    % boundary of DD PML

    %  Dirichlet BC on boundaries of DD PML
    i1= [1 nloca(1)*(nloca(2)+1)+1 1 nloca(2)+1];
    di= [1 1 nloca(2)+1 nloca(2)+1];
    dn= [nloca(2)+1 -(nloca(2)+1) 1 -1];
    nbn= [nloca(2)+1 nloca(2)+1 nloca(1)+1 nloca(1)+1];
    mb= [3 4; 3 4; 1 2; 1 2];
    for m= 1:4
        if ictype(m)~=4
            continue;
        end
        if pmlddctx.dirichlet       % artificial boundary
            ii= i1(m)+(ictype(mb(m,1))<0)*di(m):di(m):i1(m) + ...
                (nbn(m)-1-(ictype(mb(m,2))<0)*1)*di(m);
            if tgv==1
                A= setrows0(A,ii,1);
                Aaug= setrows0(Aaug,ii,1);
            else
                A= setsparse(A,ii,ii,tgv);
                Aaug= setsparse(Aaug,ii,ii,tgv);
            end
        end
        if ictype(mb(m,1))<0 && bctype(mb(m,1))==2 % extension of phys. boundary
            ii= i1(m):dn(m):i1(m)+mdpml(m)*dn(m);
            if tgv==1
                A= setrows0(A,ii,1);
                Aaug= setrows0(Aaug,ii,1);
            else
                A= setsparse(A,ii,ii,tgv);
                Aaug= setsparse(Aaug,ii,ii,tgv);
            end
        end
        if ictype(mb(m,2))<0 && bctype(mb(m,2))==2
            ii= i1(m)+(nbn(m)-1)*di(m):dn(m):i1(m)+(nbn(m)-1)*di(m) + ...
                mdpml(m)*dn(m);
            if tgv==1
                A= setrows0(A,ii,1);
                Aaug= setrows0(Aaug,ii,1);
            else
                A= setsparse(A,ii,ii,tgv);
                Aaug= setsparse(Aaug,ii,ii,tgv);
            end
        end
    end
end


%   recover original physical Dirichlet b.c. at corner with interface
ib= [];
if bounds(1)==1 && bctype(1)==2
    if ictype(3)>-.5
        ib= [ib inode1(1)];
    end
    if ictype(4)>-.5
        ib= [ib inode1(4)];
    end
end
if bounds(2)==n(1) && bctype(2)==2
    if ictype(3)>-.5
        ib= [ib inode1(2)];
    end
    if ictype(4)>-.5
        ib= [ib inode1(2)+nloc(2)];
    end
end
if bounds(3)==1 && bctype(3)==2
    if ictype(1)>-.5
        ib= [ib inode1(1)];
    end
    if ictype(2)>-.5
        ib= [ib inode1(2)];
    end
end
if bounds(4)==n(2) && bctype(4)==2
    if ictype(1)>-.5
        ib= [ib inode1(4)];
    end
    if ictype(2)>-.5
        ib= [ib inode1(2)+nloc(2)];
    end
end
if ~isempty(ib)
    if ~isempty(pmlctx)
        ib= local2global(nloca,plbounds,ib);
    end
    if tgv==1
        A= setrows0(A,ib,1);
    else
        A= setsparse(A,ib,ib,tgv);
        % FIXME: what we do with Aaug, Aneum?
    end
end

end

function iig = local2global(n,bounds,ii)
% convert local indices of nodes in subdomain to global indices
    nl= [bounds(2)-bounds(1),bounds(4)-bounds(3)]+1;
    [iiy iix]= ind2sub([nl(2)+1,nl(1)+1],ii);
    iig= (iix+bounds(1)-2)*(n(2)+1) + iiy+bounds(3)-1;
end