function [ib,jb,vb] = asmrobin(nbe,hbe,iele1,dispele,inode1,dispnode,a,w,p,dispw,pmlctx,npml)
%asmrobin -- assembly for Robin b.c. on one straight boundary
%
%  [ib,jb,vb] = asmrobin(nbe,hbe,iele1,dispele,inode1,dispnode,a,w,p,dispw)
%  computes [i,j,v] of (sub)domain matrix for Robin b.c. on one side of the
%  rectangle domain.  Note that before adding the outputs to the (sub)domain
%  matrix, one should make sure the matrix is having Neumann b.c. accounted for.
%
%
%  OUTPUTS
%
%  ib,jb,vb -- 4-by-nbe arrays for the sparse (sub)domain matrix, indexed
%  globally
%
%  INPUTS
%
%  nbe -- number of boundary elements
%
%  hbe -- mesh size along boundary
%
%  iele1 -- index in global domain of the volume element that is adjacent to
%  the first boundary element
%
%  dispele -- displacement in global domain of the volume element that is
%  adjacent to the next boundary element
%
%  inode1 -- index in global domain of the 1st boundary node
%
%  dispnode -- displacement in global domain of the next boundary node
%
%  w -- array of wavenumber values, order by index in global domain
%
%  p -- Robin-like parameters, as function of w
%
%  dispw -- displacement of 1-D index to get w on the other side

%   special case that nbe is zero, only one node
if nbe==0
    if exist('dispw','var') && ~isempty(dispw)
        we= [w(iele1)/sqrt(a(iele1)); w(iele1+dispw)/sqrt(a(iele1+dispw))];
        rhoe= [1/a(iele1); 1/a(iele1+dispw)];
        pp= p([we, rhoe]);
    else
        pp= p(w(iele1)/sqrt(a(iele1)));    
    end
    if dispnode~=0
        ib= [inode1;inode1];
        jb= [inode1;inode1+dispnode];
        vb= pp(1)*[2;1]*hbe/6.0+pp(2)*[1;-1]/hbe;
    else
        ib= inode1; jb= ib; vb= pp(1)*hbe/3.0+pp(2)/hbe;
    end
    vb= a(iele1)*vb;
    return;
end


%   boundary element stiffness and mass matrices
Kbe= [1; -1; -1; 1]/hbe;
Mbe= [2; 1; 1; 2]*hbe/6.0;

%   for every boundary element, compute [ib,jb,vb]
ib= zeros(4,nbe); jb= ib; vb= ib;
if ~exist('pmlctx','var') || isempty(pmlctx)
    for ibe= 1:nbe
        iie = iele1+(ibe-1)*dispele;
        ae= a(iie);
        % turn to PDE:  rho*div(1/rho*grad u) + w^2 u,  in function p
        if exist('dispw','var') && ~isempty(dispw)
            we= [w(iie)/sqrt(ae); w(iie+dispw)/sqrt(a(iie+dispw))];
            rhoe= [1/ae; 1/a(iie+dispw)];
            pp= p([we, rhoe]);
        else
            pp= p(w(iie)/sqrt(ae));
        end
        ib(:,ibe)= inode1+(ibe-1)*dispnode+[0;0;dispnode;dispnode];
        jb(:,ibe)= inode1+(ibe-1)*dispnode+[0;dispnode;0;dispnode];
        % assume BC:  a Dn u + p0 u - p2 * Dy (a Dy u),  in function p
        vb(:,ibe)= pp(1)*Mbe + pp(2)*ae*Kbe;
    end
else% in PML, use Robin parameters as for the reference physical element
    % converges slowly for PML, not converge for CRBC
    % TODO: using different Robin parameters in PML?
    for ibe= 1:nbe
        if ibe>npml(1) && ibe<=nbe-npml(2)
            iie= iele1+(ibe-npml(1)-1)*dispele;
        elseif ibe<=npml(1)
            iie= iele1;            
        elseif ibe>nbe-npml(2)
            iie= iele1+(nbe-npml(1)-npml(2)-1)*dispele;            
        end
        ae = a(iie);
        if exist('dispw','var') && ~isempty(dispw)
            we= [w(iie)/sqrt(ae); w(iie+dispw)/sqrt(a(iie+dispw))];
            rhoe= [1/ae; 1/a(iie+dispw)];
            pp= p([we, rhoe]);
        else
            pp= p(w(iie)/sqrt(ae));
        end
        ib(:,ibe)= inode1+(ibe-1)*dispnode+[0;0;dispnode;dispnode];
        jb(:,ibe)= inode1+(ibe-1)*dispnode+[0;dispnode;0;dispnode];
        % assume BC:  a Dn u + p0 u - p2 * Dy (a Dy u),  in function p
        vb(:,ibe)= pp(1)*Mbe + pp(2)*ae*Kbe;
    end
end

end % end of this function