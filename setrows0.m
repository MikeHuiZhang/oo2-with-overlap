function A = setrows0(A,rows,diags)
%setrows0 -- zeroing rows of a sparse matrix
%
%  A = setrows0(A,rows,diags) zeroing rows of the sparse matrix A and
%  optionally put values in diags on the diagonal entries.  
%
%  A -- the sparse matrix, INOUT
%  
%  rows -- the rows of A to be zeroed
%
%  diags -- optional values to be put on diagonal entries

[ii jj]= find(A);
idxofzero= ismember(ii,rows);
A= setsparse(A,ii(idxofzero),jj(idxofzero),0);
if nargin>2 && (length(diags)==length(rows) || length(diags)==1)
    A= setsparse(A,rows,rows,diags);
end
    
