function b= rhs(domain,n,h,scpoints,Sp,sclines,Sl,Sa,bctype,gD,tgv) 
%rhs -- Compute the right hand side on the Q1 element mesh, assuming no
%contribution from boundary conditions
%
%  b = rhs(domain,n,h,scpoints,Sp,sclines,Sl,Sa)
%
%  domain -- [xl xr; yl yr]
% 
%  n  -- n(1), n(2) number of elements in the two dimensions
%
%  h  -- h(1), h(2) mesh size in the two dimensions
% 
%  scpoints -- coordinates of point sources, e.g. (x1,y1;x2,y2)
%
%  Sp -- density of point sources, e.g. Sp(1)*Delta(x-x1,y-y1)
%
%  sclines -- locations of line sources, each line by (x1,y1,x2,y2)
% 
%  Sl -- density of line sources, assumed constant
%
%  Sa -- area spread and integrable source function, Sa(x,y)

%   initialization
b= zeros((n(2)+1)*(n(1)+1),1);
xl= domain(1,1);  yl= domain(2,1);

%   point sources
if ~isempty(scpoints)
    [ix,iy]= findscp(n,h,domain,scpoints);
    xx= (scpoints(:,1)-xl-h(1)*(ix-1))/h(1);
    yy= (scpoints(:,2)-yl-h(2)*(iy-1))/h(2);
    inode1= (ix-1)*(n(2)+1) + iy;
    b(inode1)= b(inode1) + Sp.*(1-xx).*(1-yy);
    b(inode1+1)= b(inode1+1) + Sp.*(1-xx).*yy;
    b(inode1+n(2)+1)= b(inode1+n(2)+1) + Sp.*xx.*(1-yy);
    b(inode1+n(2)+2)= b(inode1+n(2)+2) + Sp.*xx.*yy;
end

%   line sources
if exist('Sl','var') && ~isempty(sclines)
    ix= 1+floor((sclines(:,[1,3])-xl)/h(1));
    iy= 1+floor((sclines(:,[2,4])-yl)/h(2));
    ix= (ix>n(1))*n(1) + (ix<=n(1)).*ix;
    for m= 1:size(sclines,1)
        if ix(m,1)<=ix(m,2)
            xx= xl+(ix(m,1)-1:ix(m,2))*h(1);
        else
            xx= xl+(ix(m,1):-1:ix(m,2)-1)*h(1);
        end
        xx(1)= sclines(m,1); xx(end)= sclines(m,3);
        if iy(m,1)<=iy(m,2)
            yy= yl+(iy(m,1)-1:iy(m,2))*h(2);
        else
            yy= yl+(iy(m,1):-1:iy(m,2)-1)*h(2);
        end
        yy(1)= sclines(m,2); yy(end)= sclines(m,4);
        if yy(1)==yy(end)
            yall= zeros(size(xx));
            yall(:)= yy(1);
            xall= xx;
        elseif xx(1)==xx(end)
            yall= yy;
            xall= zeros(size(yy));
            xall(:)= xx(1);
        else
            yall= [yy yy(1)+(xx-xx(1))*(yy(end)-yy(1))/(xx(end)-xx(1))];
            xall= [xx xx(1)+(yy-yy(1))*(xx(end)-xx(1))/(yy(end)-yy(1))];
            xall= unique(xall);  yall= unique(yall);
            if ix(m,1)<=ix(m,2)
                xall= sort(xall);
            else
                xall= sort(xall,2,'discend');
            end
            if iy(m,1)<=iy(m,2)
                yall= sort(yall);
            else
                yall= sort(yall,2,'discend');
            end
        end
        xmean= xall(1:end-1)+diff(xall)/2;
        ymean= yall(1:end-1)+diff(yall)/2;
        iix= 1+floor((xmean-xl)/h(1));
        iiy= 1+floor((ymean-yl)/h(2));
        inode1= (iix-1)*(n(2)+1)+iiy;
        x1= (xall(1:end-1)-iix*h(1)) /h(1);  x1= x1(:); 
        x2= (xall(2:end)-iix*h(1)) /h(1);  x2= x2(:);
        y1= (yall(1:end-1)-iiy*h(2)) /h(2);  y1= y1(:);
        y2= (yall(2:end)-iiy*h(2)) /h(2);  y2= y2(:);
        dist12h= sqrt((x1-x2).^2+(y1-y2).^2) /2;
        b(inode1)= b(inode1)+Sl(m)*dist12h.*abs((1-x1).*(1-y1)-(1-x2).*(1-y2));
        b(inode1+1)= b(inode1+1)+Sl(m)*dist12h.*abs((1-x1).*y1-(1-x2).*y2);
        b(inode1+n(2)+1)= b(inode1+n(2)+1)+Sl(m)*dist12h.*abs((1-y1).*x1-(1- ...
                                                          y2).*x2);
        b(inode1+n(2)+2)= b(inode1+n(2)+2)+Sl(m)*dist12h.*abs(y1.*x1-y2.*x2);
    end
end

%   area source
if exist('Sa','var') && ~isempty(Sa)
    ix= 1:n(1);  iy= 1:n(2);  [ix, iy]= meshgrid(ix,iy);  
    ix= ix(:);  iy= iy(:);
    b=addsca(b,ix,iy,n,h,xl,yl,Sa);
end

%  boundary conditions
inode1= [1, n(1)*(n(2)+1)+1, 1, n(2)+1];
dispnode= [1, 1, n(2)+1, n(2)+1];
nbe= [n(2), n(2), n(1), n(1)];
hbe= [h(2), h(2), h(1), h(1)];
x0= [xl domain(1,2) xl xl];
y0= [yl yl yl domain(2,2)];
dx= [0 0 h(1) h(1)];
dy= [h(2) h(2) 0 0];

%   Dirichlet data
if exist('gD','var') && ~isempty(gD)
    for m= 1:4
        if bctype(m)~=2
            continue;
        end
        rows= inode1(m):dispnode(m):inode1(m)+nbe(m)*dispnode(m);
        x= x0(m)+(0:nbe(m))*dx(m); y= y0(m)+(0:nbe(m))*dy(m); x= x(:); y= y(:);
        b(rows)= gD(x,y)*tgv;
    end
end

end                                     % end function rhs