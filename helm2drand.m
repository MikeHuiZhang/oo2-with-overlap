%helm2drand.m
%
% The Helmholtz problem in a rectrangle:
%
%   - div(a grad u) - w^2 u = S,  in \Omega= (xl,xr) X (yl,yr)
%
%                     PML u = 0, PML/auxiliary, type 4
%
%     (a D_n + sqrt(Dss)) u = 0, sqrt approx. of DtN, type 3
%
%                         u = gD, Dirichlet, type 2
%
%             (a D_n + T) u = gR, Robin-like b.c., type 1
%
%                   a D_n u = gN, Neumann b.c., type 0
%
%            conditions at corners of two second-order Robin-like b.c.
%
% where w= 2*pi*f/c.  We assume c and a are constant on each element,
% otherwise we average them.  We use e.g. bctpye= [1 1 2 0] to describe the
% b.c. type on the left, right, bottom, top sides.  The tangential operator
% T in the absorbing b.c. is decribed in second-order form:
%
%   T = a*(p1 - p2 D_tt),
%
% with p1, p2 complex-valued and constant on each element, e.g. the
% second-order absorbing condition is with p1= sgn*1i*w, p2= -sgn*0.5i/w and
% the first-order absorbing condition is with p1= sgn*1i*w, p2= 0, where sgn
% is the conventional sign with the value 1, or -1.  The condition at a
% corner of two second-order Robin-like boundary is given by
%
%     (D_n1 + D_n2 + p3) u = gC,
%
% with p3 complex-valued and constant on each corner element.  The right-hand
% sides gR, gN and gC are actually taken as zeros for the moment.  The source
% term S consists of a few point sources, and a integrable function.  The point
% sources are described by locations and strength, i.e. Sp*Delta(x-xs,y-ys).
%
% For discretization, we use a uniform cartesian mesh and Q1 basis.

function [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw)

%%  Direct solver
%   define the continuous problem
%kconst;
marmousi;
bctype= [4 4 2 2];                      % left, right, bottom, top sides
sgn= 1;                                 % sign convention
p= @(w) sgn*1i*[w -.5/w 1.5*w];         % parameters for Robin-like b.c.
pmlctx.nl= npml; pmlctx.method= 'pml';     % 'pml', 'pole', or 'crbc'
pmlctx.sgn= sgn;  pmlctx.dirichlet= true; pmlctx.H= diff(myDomain,1,2);
pmlctx.param= 1; 
pmlctx.quadN= 7;   
pmlctx.C= 8*pi;    
pmlctx= initpml(pmlctx);

%   mesh parameters
n= ceil(pmlctx.H./h1)                  % numbers of elements in x and y
h= diff(myDomain,1,2)./n


%   arrays of density and wavenumber
if isempty(file4a)
    a= ones(n(2),n(1));                 % first y, then x
else
    a= fread2mesh(file4a,asize,n);
end
if isempty(file4vel)
    c= ones(n(2),n(1));
    w= 2*pi*f./c;          % first y, then x
else
    w= fread2mesh(file4vel,velsize,n,fileform4vel);
    w= 2*pi*f./w;
end

%   constant a and w for PML along four sides
pmlctx.aref = mean(a(:));
pmlctx.wref = mean(w(:))

%   linear system
tgv= 1;                                 % 1 or large number
[A,R,nall,RD]= assembly(n,h,a,w,bctype,p,tgv,pmlctx);% matrix
%b= rhs(myDomain,n,h,scpoints,Sp,sclines,Sl,Sa,bctype,gD,tgv);  % right-hand
                                                             % side
rng(2); udirect= rand(size(A,2),1); b= A*udirect;
if ~exist('udirect','var') && ~isempty(R)
    b= R.'*b;
end
uD= RD*b; b= b - A*(uD/tgv);% homogenize Dirichlet: important for oos.m

%   direct solver
solbegin = tic;
%udirect= A\b;  udirect= udirect + uD/tgv;
disp('direct solver time: ');
soltime = toc(solbegin)
if ~isempty(R)
    upml = udirect - uD;
    udirect= R*udirect; uD= R*uD;  
else
    upml = [];
end
udirect= reshape(udirect,n(2)+1,n(1)+1);

x= myDomain(1,1):h(1):myDomain(1,2);  y= myDomain(2,1):h(2):myDomain(2,2);
% $$$ figure, surf(x,y,real(udirect));
% $$$ set(findobj(gca,'Type','Surface'),'EdgeColor','none'), axis equal, colorbar
% $$$ set(gca,'YDir','reverse'); colormap jet;
% $$$ xlabel('x'); ylabel('y'); title('udirect');
%errABC;

udirect(:) = udirect(:) - uD/tgv;       % remove Dirichlet
%%   optimized Schwarz methods
% AA is used to extract subdomain matrices, empty for using A
ww= (1-sgn*1i/(2.0*pi*f))*w;            % modified wavenumber
AA= [];%assembly(n,h,a,ww,bctype,p,tgv,pmlctx);
iterin= 1;                              % iteration numbers of Schwarz for one application of the preconditioner
N= [Nx Ny];                             % number of subdomains in x and y
o= [ox oy];                             % number of elements to extend
popt= @getpopt;                         % c.f. getpopt.m
u= zeros(size(b));                      % zero initial guess
%rng(2); u= rand(size(b)); u= u-RD*u;  % random initial guess
maxit= 200; tol= 1e-12;
funnames= {fun};
prolongation= 'restricted';             % 'restricted','full','unity'
itermethods= {'gmres'}; % gmres (residual is neither physical nor original but
                        % of the preconditioned system) or richardson
linetype= {'r-.','b-+'};
gamma= 1;                               % h= C/w^gamma for getpopt
H= diff(myDomain,1,2)./N(:);
optctx= {sgn,dw,[],[],gamma,[],[],ictx,H,N(:),o(:),h(:),[],[],[],[],[]};% c.f. getpopt, init_optctx
if isempty(ictx2)
    optctx2 = [];
else
    optctx2= optctx; optctx2{8}= ictx2;
end
pmlddctx = pmlctx; pmlddctx.H = pmlctx.H./N(:); pmlddctx.nl = npmldd;
%pmlddctx.param = 1; pmlddctx.C = 8*pi;
matctx= {a,w,p,tgv,h,bctype,popt,0,[],pmlctx,R,nall,pmlddctx};
rhsctx= {};
oostime= tic;
[udd,ittime,flag,relres,iter,resvec,maxerr,errvec,deltau]= oos(A,AA,iterin,b,n,N,o,prolongation,matctx,rhsctx,optctx,[],funnames,itermethods,u,maxit,tol,udirect,optctx2,upml);
oostime= toc(oostime)
maxerr
iter = iter(2)
relres
flag
res = norm(A*udd - b,2)

% $$$ opts= optimset('MaxFunEvals',9999);
% $$$ [pp gmin] = fminsearch(@goal,[95.3976 1.3346 -7.5468 -0.0709])
% $$$ function re= goal(pp)
% $$$     popt2= @(w,optctx,m) [pp(1)*1i,-pp(2)*1i/w,0];
% $$$     popt= @(w,optctx,m) [pp(3)*1i,-pp(4)*1i/w,0];
% $$$     matctx= {a,ww,p,tgv,h,bctype,popt,stretch,popt2};
% $$$     rhsctx= {};
% $$$     %maxit= 60;
% $$$     tic
% $$$     [flag,relres,iter,resvec,maxerr]= oos(A,A,iterin,b,n,N,o,prolongation,matctx, ...
% $$$                                           rhsctx,optctx,cpctx,funnames, ...
% $$$                                           itermethods,u,maxit,tol,udirect);
% $$$     disp('oos total time');
% $$$     toc
% $$$     maxerr
% $$$     iter
% $$$     re= relres;
% $$$ end

% $$$ [p0,p2]= meshgrid(20:20:200,0:0.01:1); gv= ones(size(p0));
% $$$ for m= 1:length(p0(:))                  % direct contour
% $$$     popt= @(w,optctx,m) [p0(m),p2(m),0];
% $$$     popt2= @(w,optctx,m) [0,1,0];
% $$$     matctx= {a,ww,p,tgv,h,bctype,popt,stretch,popt2};
% $$$     rhsctx= {};
% $$$     tic
% $$$     [flag,relres,iter,resvec,maxerr]= oos(A,A,iterin,b,n,N,o,prolongation,matctx, ...
% $$$                                           rhsctx,optctx,cpctx,funnames, ...
% $$$                                           itermethods,u,maxit,tol,udirect);
% $$$     disp('oos total time');
% $$$     toc
% $$$     maxerr
% $$$     iter
% $$$     gv(m)= iter(2);
% $$$ end
% $$$ figure, [ch,hh]= contour(p0,p2,log2(gv)); clabel(ch,hh);
% $$$ xlabel('Re'), ylabel('Im'); [gvmin,imin]= min(gv(:));
% $$$ hold on, plot(p2(imin),p0(imin),'*'),
% $$$ text(p2(imin)-1.2,p0(imin),['(',num2str(p2(imin)),', ', ...
% $$$                     num2str(p0(imin)),')'],'FontSize',11);
% $$$ hold off;

% $$$ figure;
% $$$ if iscell(resvec)
% $$$     for m= 1:length(resvec)
% $$$         semilogy(0:iter(m,end),resvec{m}(1:iter(m,end)+1),linetype{m});
% $$$         if m==1
% $$$             hold on;
% $$$             xlabel('iterations'), ylabel('residuals');
% $$$         end
% $$$         legstr{m}= [funnames{m}, ' ', itermethods{m}];
% $$$     end
% $$$     legend(legstr{:});
% $$$ else
% $$$     semilogy(0:iter(1,end),resvec(1:iter(1,end)+1),linetype{1});
% $$$     xlabel('iterations'), ylabel('residuals');
% $$$     legend([funnames{1},'+', itermethods{1}]);
% $$$ end

end