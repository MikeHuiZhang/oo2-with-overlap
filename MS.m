function [u,res] = MS(uinit,b,A,Ro,Rt,As,symmetric,Rt2,AA,iterin,Ro2,As2)
%MS -- Multiplicative Schwarz iteration operator
%
%  [u,res] = MS(uinit,b,A,Ro,Rt,As,symmetric)
%
% Rt is [ ), Rt2 is ( ] useful for non-overlapping MSS

%  init
global resu erru deltau oldu Arows Arows2;
Ns= length(As);
u= uinit;

if ~exist('Rt2','var')
    Rt2= [];
end
if ~exist('Ro2','var')
    Ro2= [];
end
if ~exist('As2','var')
    As2= [];
end
if isempty(Arows)    
    Arows = cell(Ns,1);
    if exist('AA','var') && ~isempty(AA)
        for s = 1:Ns
            Arows{s} = Ro{s}*AA;
        end
    else
        for s = 1:Ns
            Arows{s} = Ro{s}*A;
        end
    end
end
if ~isempty(Ro2) && isempty(Arows2)
    Arows2 = cell(Ns,1);
    if exist('AA','var') && ~isempty(AA)
        for s = 1:Ns
            Arows2{s} = Ro2{s}*AA;
        end
    else
        for s = 1:Ns
            Arows2{s} = Ro2{s}*A;
        end
    end
end

function Inner(Ro2,Ars,A2,Rt2)
    for iter= 1:iterin                  % solve AA du = r
        for s= 1:Ns-1
            us= Ro2{s}*r-Ars{s}*du;
            us= A2{s}\us;
            du= du + Rt2{s}*us;
        end
        if nargin>6 && ~isempty(symmetric) && symmetric
            for s= Ns:-1:1
                us= Ro{s}*r-Ars{s}*du;
                us= As{s}\us;
                du= du + Rt{s}*us;
            end
        end
    end
end

if exist('AA','var') && ~isempty(AA)
    if ~exist('iterin','var')
        iterin= 1;
    end
    r= b-A*u;
    du= zeros(size(u));
    %  a shortcoming of Matlab: no macro, no shallow copy
    if isempty(Rt2)
        if isempty(Ro2)
            if isempty(As2)
                Inner(Ro,Arows,As,Rt);
            else
                Inner(Ro,Arows,As2,Rt);
            end
        else
            if isempty(As2)
                Inner(Ro2,Arows2,As,Rt);
            else
                Inner(Ro2,Arows2,As2,Rt);
            end
        end
    else
        if isempty(Ro2)
            if isempty(As2)
                Inner(Ro,Arows,As,Rt2);
            else
                Inner(Ro,Arows,As2,Rt2);
            end
        else
            if isempty(As2)
                Inner(Ro2,Arows2,As,Rt2);
            else
                Inner(Ro2,Arows2,As2,Rt2);
            end
        end
    end
    u= u+du;
    if nargout>1
        res= norm(b-A*u);
    end
    return;
end

function Forward(Ro2,Ars2,A2,Rt2)
    for s= 1:Ns-1
        us= Ro2{s}*b-Ars2{s}*u;
        us= A2{s}\us;
        u= u + Rt2{s}*us;
    end
end

if isempty(Rt2)
    if isempty(Ro2)
        if isempty(As2)
            Forward(Ro,Arows,As,Rt);
        else
            Forward(Ro,Arows,As2,Rt);
        end
    else
        if isempty(As2)
            Forward(Ro2,Arows2,As,Rt);
        else
            Forward(Ro2,Arows2,As2,Rt);
        end
    end
else
    if isempty(Ro2)
        if isempty(As2)
            Forward(Ro,Arows,As,Rt2);
        else
            Forward(Ro,Arows,As2,Rt2);
        end
    else
        if isempty(As2)
            Forward(Ro2,Arows2,As,Rt2);
        else
            Forward(Ro2,Arows2,As2,Rt2);
        end
    end
end

if nargin>6 && ~isempty(symmetric) && symmetric
    for s= Ns:-1:1                      % for source transfer down to s=1,
        us= Ro{s}*b-Arows{s}*u;         % while for others down to s=2 is
        us= As{s}\us;                   % enough because the next forward     
        u= u + Rt{s}*us;                % sweep solve the same sub-problem
    end
end

if nargout>1
    res= norm(b-A*u);
end


end