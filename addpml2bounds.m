function boundswithpml = addpml2bounds(bounds,N,sx,sy,nl,bctype)
%ADDPML2BOUNDS computes new bounds (counted in elements) of subdomain after
% PML added to the original domain.  The same PML is also assigned to
% subdomains adjacent to the boundary of the original domain. But every
% subdomain has changed bounds when counted in the PML-augmented domain.
%
% INPUTS
%
% bounds -- element inclusive bounds in the original domain, in x, y
%
% N -- number of subdomains in x,y
%
% sx,sy -- 2-D subscript of the subdomain in x,y, 1 ~ N(1), 1 ~ N(2)
%
% nl -- scalar, number of PML elements
%
% bctype -- type of BC on four sides of the original rectangular domain, 4
% for PML
%
% OUTPUT
%
% boundswithpml -- compute the element bounds of the new subdomain (with
% possible PML) in the new domain with PML
%
boundswithpml= bounds;
boundswithpml(1:2)= boundswithpml(1:2)+(bctype(1)==4)*nl;
boundswithpml(3:4)= boundswithpml(3:4)+(bctype(3)==4)*nl;
boundswithpml(1)= boundswithpml(1) - (bctype(1)==4).*(sx==1)* nl;
boundswithpml(3)= boundswithpml(3) - (bctype(3)==4).*(sy==1)* nl;
boundswithpml(2)= boundswithpml(2) + (bctype(2)==4).*(sx==N(1))* nl;
boundswithpml(4)= boundswithpml(4) + (bctype(4)==4).*(sy==N(2))* nl;


