function [gp2d,gw2d]= getgpw2d()
% for use by stima2d.m and rhs2d.m
% Gauss quadrature points and weights on [-1,1]^2, generated by symint.m

% 4 order
gp2d = ...
      [-0.8611   -0.8611
       -0.8611   -0.3400
       -0.8611    0.8611
       -0.8611    0.3400
       -0.3400   -0.8611
       -0.3400   -0.3400
       -0.3400    0.8611
       -0.3400    0.3400
        0.8611   -0.8611
        0.8611   -0.3400
        0.8611    0.8611
        0.8611    0.3400
        0.3400   -0.8611
        0.3400   -0.3400
        0.3400    0.8611
        0.3400    0.3400];
gw2d = ...
      [ 0.1210
        0.2269
        0.1210
        0.2269
        0.2269
        0.4253
        0.2269
        0.4253
        0.1210
        0.2269
        0.1210
        0.2269
        0.2269
        0.4253
        0.2269
        0.4253];