function vb = pmlbmat(jp,a,w,pmlctx)
% Neumann boundary nodes interaction
%
% PML | <- assembly along this boundary
% ----| 
% Phy.| <- (as above) 
% ----|  
% PML | <- (as above)
% ----|
%
% So the PML stretching axis is parallel to the boundary 

if pmlctx.axi==1
    if isfield(pmlctx,'aref') && isfield(pmlctx,'wref')
        [s,pts,wts]= pmlctx.sx(jp,pmlctx.aref,pmlctx.wref, pmlctx);
    else
        [s,pts,wts]= pmlctx.sx(jp,a,w,pmlctx);
    end
    h= pmlctx.h;
else
    if isfield(pmlctx,'aref') && isfield(pmlctx,'wref')
        [s,pts,wts]= pmlctx.sy(jp,pmlctx.aref,pmlctx.wref, pmlctx);
    else
        [s,pts,wts]= pmlctx.sy(jp,a,w,pmlctx);
    end
    h= [pmlctx.h(2),pmlctx.h(1)];
end
if jp~=0
    if length(s)==1
        K1= [1 -1]*s/h(1); M1= [1/3 1/6]/s*h(1); M1a= M1;
    else
        K1= [1 -1]*sum(wts.*s)/h(1);
        if jp<0
            M1= [sum(wts.*pts.*pts./s), sum(wts.*pts.*(1-pts)./s)]*h(1);
            M1a= [sum(wts.*(1-pts).*(1-pts)./s)*h(1), M1(2)];
        else
            M1= [sum(wts.*(1-pts).*(1-pts)./s), sum(wts.*pts.*(1-pts)./s)]*h(1);
            M1a= [sum(wts.*pts.*pts./s)*h(1), M1(2)];
        end
    end
else
    warning('pmlbmat: 1st arg is zero (not in pml), return Q1-element vals');
    K1= [1 -1]/h(1); M1= [1/3 1/6]*h(1); M1a= M1;
end
K2= [1 -1]/h(2); M2= [1/3 1/6]*h(2);
Ks= K1.'*M2 + M1.'*K2; Ks= Ks(1:2,1); Ms= M1.'*M2; Ms= Ms(1:2,1);
Ksa= K1.'*M2 + M1a.'*K2; Ksa= Ksa(1:2,1); Msa= M1a.'*M2; Msa= Msa(1:2,1);
vb= a*[Ks;Ksa] - w^2*[Ms;Msa];
