function pmlctx = initpml(pmlctx)

%   Switch methods of pmlctx.
switch pmlctx.method
  case 'crbc'
    pmlctx.ematv= @crbcematv; pmlctx.sx= @crbcs; pmlctx.sy= @crbcs;
    pmlctx.bmat= @crbcbmat;
  case 'pole'
    pmlctx.ematv= @polematv; pmlctx.bmat= @polebmat; pmlctx.sx= @poles;
    pmlctx.sy= @poles;
  otherwise
    pmlctx.ematv= @pmlematv; pmlctx.sx= @pmls; pmlctx.sy= @pmls;
    pmlctx.bmat= @pmlbmat;
end