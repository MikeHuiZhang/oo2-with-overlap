function [A,R,nall,RD]= asmpml(n,h,a,w,bctype,tgv,pmlctx,w4pml)
%asmpml -- Assemble the matrix for the Helmholtz with PML.
%
%  pmlctx.ematv -- Different methods describes in different way the element
%  matrix in pml.

%   Inputs check
if ~exist('pmlctx','var')
    error('asmpml: input arg pmlctx is missing');
end
if isempty(pmlctx)
    error('asmpml: empty arg pmlctx');
end

%   Count sizes with pml.
nall= n;                                % number of elements in x,y
phy= [1 n(1) 1 n(2)];
if bctype(1)==4
    phy(1)= pmlctx.nl+1;                % inclusive bound of physical elements
    nall(1)= nall(1)+pmlctx.nl;
end
phy(2)= nall(1);
if bctype(2)==4
    nall(1)= nall(1)+pmlctx.nl;
end
if bctype(3)==4
    phy(3)= pmlctx.nl+1;
    nall(2)= nall(2)+pmlctx.nl;
end
phy(4)= nall(2);
if bctype(4)==4
    nall(2)= nall(2)+pmlctx.nl;
end
pmlctx.h = h; pmlctx.H = n(:).*h(:);

%   Compute the matrix R for restriction to physical domain.
ix= phy(1):phy(2)+1; iy= phy(3):phy(4)+1;
[ix iy]= meshgrid(ix,iy); jj= (ix-1)*(nall(2)+1)+iy; jj= jj(:);
R= sparse(1:length(jj),jj,1,length(jj),(nall(1)+1)*(nall(2)+1));

%   Set element stiffness and mass stencils in physical domain.
%     2  3
%     | /   
%     0--1
hxy= h(1)/h(2);  hyx= 1/hxy;
Ks= [hxy+hyx, 0.5*hxy-hyx, 0.5*hyx-hxy, -0.5*(hxy+hyx)]/3.0;
Ms= [1/9.0, 1/18.0, 1/18.0, 1/36.0]*h(1)*h(2);

%   Compute [i j v] for every cell.
%    4--3
%    |  |
%    1--2
ii= zeros(16,nall(1)*nall(2)); jj= ii; vv= ii; I= [1:4, 1:4, 1:4, 1:4];
for ix= 1:nall(1)
    for iy= 1:nall(2)
        ie= (ix-1)*nall(2)+iy; i1= (ix-1)*(nall(2)+1)+iy;
        ii(1:4,ie)= i1;
        ii(5:8,ie)= i1+nall(2)+1;
        ii(9:12,ie)= i1+nall(2)+2;
        ii(13:16,ie)= i1+1;
        jj(1:4,ie)= i1+[0; nall(2)+1; 1; nall(2)+2];
        jj(5:8,ie)= i1+[nall(2)+1; 0; nall(2)+2; 1];
        jj(9:12,ie)= i1+[nall(2)+2; 1; nall(2)+1; 0];
        jj(13:16,ie)= i1+[1;nall(2)+2;0;nall(2)+1];
        ixr= min(max(phy(1),ix),phy(2)); iyr= min(max(phy(3),iy),phy(4));
        ixp= ixr-phy(1)+1; iyp= iyr-phy(3)+1;
        % Compute the 2-D element matrix
        if ixr~=ix || iyr~=iy
            if exist('w4pml','var') && ~isempty(w4pml)
                wele= w4pml(iyp,ixp);
            else
                wele= w(iyp,ixp);
            end
            aele= a(iyp,ixp);
            vv(1:16,ie)= pmlctx.ematv(ix-ixr,iy-iyr,aele,wele,pmlctx,pmlctx);
        else
            vv(1:16,ie)= a(iyp,ixp)*Ks(I) - w(iyp,ixp)^2*Ms(I);
        end        
    end
end

%    Setup the matrix A.
A= sparse(ii,jj,vv,(nall(1)+1)*(nall(2)+1),(nall(1)+1)*(nall(2)+1));

%    Treat BC on PML boundaries that are not continuation of physical
%    boundaries.

% homogeneous Dirichlet on boundary of PML, corners with physical boundary are
% left unchanged
dirichlet= zeros(2*nall(1)+2*nall(2)+4,1); count= 0;
i1= [1 nall(1)*(nall(2)+1)+1 1 nall(2)+1];
di= [1 1 nall(2)+1 nall(2)+1];
nbn= [nall(2)+1 nall(2)+1 nall(1)+1 nall(1)+1];
mb= [3 4; 3 4; 1 2; 1 2];
if pmlctx.dirichlet
    for m= 1:4
        if bctype(m)==4
            ii= i1(m)+(bctype(mb(m,1))~=4)*di(m):di(m):i1(m) + ...
                (nbn(m)-1-(bctype(mb(m,2))~=4)*1)*di(m);
            dirichlet(count+1:count+length(ii)) = ii;
            count = count + length(ii);
%             if tgv==1
%                 A= setrows0(A,ii,1);
%             else
%                 A= setsparse(A,ii,ii,tgv);
%             end            
        end
    end
end


%    Treat Robin BC on physical boundary away from PML.


%    Treat Dirichlet BC on physical boundary, including the continued parts in
%    common with boundary of PML
for m= 1:4
    if bctype(m)==2
        dirichlet(count+1:count+nbn(m))= i1(m):di(m):i1(m)+(nbn(m)-1)*di(m);
        count= count + nbn(m);
    end    
end
dirichlet= unique(dirichlet(1:count));
RD= sparse(dirichlet,dirichlet,1,size(A,1),size(A,2));
if tgv==1 && ~isempty(dirichlet)
    A= setrows0(A,dirichlet,1);
elseif ~isempty(dirichlet)
    A= setsparse(A,dirichlet,dirichlet,tgv);
end

%    Treat BC on PML modified physical boundary. (c.f. work of Gunilla Kreiss)



end