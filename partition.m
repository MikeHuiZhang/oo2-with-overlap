function [ibounds,ibounds2,obounds,isecs] = partition(n,N,o,bctype,disp2corner,edgetype)
%partition -- cartesian partition of cartesian mesh elements into subdomains
%
%  [ibounds,obounds,neighbors,isecs] = partition(n,N,o,bctype,disp2corner,edgetype)
%
%  INPUTS 
%
%  n -- n(1), n(2) elements in x and y dimensions
%
%  N -- N(1), N(2) subdomains in x and y dimensions
% 
%  o -- o(1), o(2) number of elements to be extended in x and y
%
%  bctype -- types of boundary conditions on four sides of global domain,
%  0 Neumann, 1 Robin-like, 2 Dirichlet, Dirichlet nodes will be excluded
%  from intersections, default all one's
%
%  disp2corner -- for edge intersections with left,right,up,down neighbors,
%  leave how many nodes to corner parts, in x,y directions; by default (input
%  arg absent) it is [o(1), o(2)]
%
%  edgetype -- for beginning/ending node of edge intersection, do we
%  distribute it to only one neighbor, 1: yes, 0: to more than one neighbors;
%  by default (input arg absent), in case max(o)>0 this is always 1 and in
%  case without overlap this is always 0
%
%
%  OUTPUTS
%  
%  ibounds -- ibounds(1:4,s) are ixbegin, ixend, iybegin, iyend inclusive
%  bounds for non-overlapping parition of elements for subdomain s, but ixend
%  and iyend are further decreased by 1 when it is not on the global bounds.
%
%  ibounds2 -- different from ibounds which can induce [ ) nodal bounds,
%  these can induce ( ] nodal bounds, useful for forward sweeping of
%  non-overlapping methods
%
%  obounds -- obounds(1:4,s) are x,y inclusive bounds for the overlapping
%  partition of elements for subdomain s
%  
%  isecs{s}.neighbors -- indices of neighbors of subdomain s, each number is
%  for one intersection, repeated neighbors are for multiple intersections
%      
%  isecs{s}.obnodes -- a 2-by-m array, s is subdomain, m is number of
%  intersections to neighboring subdomains, obnodes(:,m) are beginning and
%  ending nodes of the intersection, excluding exterior Dirichlet
%
%  isecs{s}.obdir -- obdir(m) is the direction of the m-th intersection, 1,-1
%  or 2,-2 to represent x or y directions, positive or negative sign is for
%  the outward normal direction which is positive y or x directions
%
%  isecs{s}.obtype -- obtype(:,m) how to treat the beginning/ending nodes, if 0,
%  it means that the self and normal interactions are splitted to more than one
%  neighbors; if 1, it means the interactions are same as it in the s-th
%  subdomain; at exterior non-Dirichlet boundary node we regard it as 1.
  
%   initialization
if nargin<3 || isempty(o)
    o= [1 1];
elseif length(o)==1
    o= [o o];
elseif sum(o(:) > floor(n(:)./N(:)))
    error('wrong arg','Bmat:overlap is too large and covers neighbors');
end
if nargin<4 || isempty(bctype)
    bctype= [1 1 1 1];
elseif sum(ismember(bctype,[0 1 2 3 4]))<4
    error('wrong arg','Bmat:input values of bctype must be 0,1,2,3, or 4');
end
if nargin<5 || isempty(disp2corner)
    disp2corner= o;                     % leave some nodes to corner parts
elseif length(disp2corner)<2
    disp2corner= [disp2corner disp2corner];
elseif sum(disp2corner(:)>o(:))
    error('wrong arg',['partition: input values of disp2corner must be less ', ...
                       'than overlap in that direction']);
end
if nargin<6 || isempty(edgetype)
    if max(o)>0
        edgetype= 1;                    % obtype for edge parts
    else
        edgetype= 0;
    end
else
    edgetype= sign(abs(edgetype));
end


%   define the subdomains-- rectangles, by elements
ixbounds= part1d(n(1),N(1));
iybounds= part1d(n(2),N(2));
ibounds= zeros(4,N(1)*N(2));
obounds= ibounds; ibounds2= ibounds;
for s= 1:N(1)*N(2)
    [sy,sx]= ind2sub([N(2),N(1)],s);
    ibounds(1,s)= ixbounds(1,sx);
    ibounds(2,s)= ixbounds(2,sx)-(sx<N(1));
    ibounds(3,s)= iybounds(1,sy);
    ibounds(4,s)= iybounds(2,sy)-(sy<N(2));
    
    ibounds2(1,s)= ixbounds(1,sx)+(sx>1);
    ibounds2(2,s)= ixbounds(2,sx);
    ibounds2(3,s)= iybounds(1,sy)+(sy>1);
    ibounds2(4,s)= iybounds(2,sy);    
    
    obounds(1,s)= max(ixbounds(1,sx)-o(1),1);
    obounds(2,s)= min(ixbounds(2,sx)+o(1),n(1));
    obounds(3,s)= max(iybounds(1,sy)-o(2),1);
    obounds(4,s)= min(iybounds(2,sy)+o(2),n(2));
end

%   intersections: partition of subdomain interface
isecs= cell(N(1)*N(2),1);
for s= 1:N(1)*N(2)
    [sy,sx]= ind2sub([N(2),N(1)],s);
    m= 1*(sx>1) + 1*(sy<N(2)) + 1*(sx<N(1)) + 1*(sy>1) + 2*(sx>1 && sy<N(2)) + ...
       2*(sx>1 && sy>1) + 2*(sx<N(1) && sy>1) + 2*(sx<N(1) && sy<N(2));
    isecs{s}.neighbors= zeros(m,1); 
    isecs{s}.obnodes= zeros(2,m);
    isecs{s}.obdir= zeros(m,1);
    isecs{s}.obtype= zeros(2,m);
    m= 0;
    if sx>1                             % left neighbor
        m= m+1;
        isecs{s}.neighbors(m)= s-N(2);
        isecs{s}.obnodes(1,m)= obounds(3,s) + disp2corner(2)*(sy>1) + ...
            1*(sy==1 && bctype(3)==2);
        isecs{s}.obnodes(2,m)= obounds(4,s) + 1 - disp2corner(2)*(sy<N(2)) - ...
            1*(sy==N(2) && bctype(4)==2);
        isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + (obounds(1,s)-1)*(n(2)+1);
        isecs{s}.obdir(m)= -2;
        isecs{s}.obtype(1,m)= edgetype*(sy>1) + 1*(sy==1);
        isecs{s}.obtype(2,m)= edgetype*(sy<N(2)) + 1*(sy==N(2));
    end
    if sx<N(1)                          % right neighbor
        m= m+1;
        isecs{s}.neighbors(m)= s+N(2);
        isecs{s}.obnodes(1,m)= obounds(3,s) + disp2corner(2)*(sy>1) + ... 
            1*(sy==1 && bctype(3)==2);
        isecs{s}.obnodes(2,m)= obounds(4,s) + 1 - disp2corner(2)*(sy<N(2)) - ...
            1*(sy==N(2) && bctype(4)==2);
        isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + obounds(2,s)*(n(2)+1);
        isecs{s}.obdir(m)= 2;
        isecs{s}.obtype(1,m)= edgetype*(sy>1) + 1*(sy==1);
        isecs{s}.obtype(2,m)= edgetype*(sy<N(2)) + 1*(sy==N(2));
    end
    if sy>1                             % down neighbor
        m= m+1;
        isecs{s}.neighbors(m)= s-1;
        isecs{s}.obnodes(1,m)= obounds(1,s) + disp2corner(1)*(sx>1) + ...
            1*(sx==1 && bctype(1)==2);
        isecs{s}.obnodes(2,m)= obounds(2,s) + 1 - disp2corner(1)*(sx<N(1)) - ...
            1*(sx==N(1) && bctype(2)==2);
        isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1) + obounds(3,s);
        isecs{s}.obdir(m)= -1;
        isecs{s}.obtype(1,m)= edgetype*(sx>1) + 1*(sx==1);
        isecs{s}.obtype(2,m)= edgetype*(sx<N(1)) + 1*(sx==N(1));
    end
    if sy<N(2)                          % up neighbor
        m= m+1;
        isecs{s}.neighbors(m)= s+1;
        isecs{s}.obnodes(1,m)= obounds(1,s) + disp2corner(1)*(sx>1) + ...
            1*(sx==1 && bctype(1)==2);
        isecs{s}.obnodes(2,m)= obounds(2,s) + 1 - disp2corner(1)*(sx<N(1)) - ...
            1*(sx==N(1) && bctype(2)==2);
        isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1) + obounds(4,s) + 1;
        isecs{s}.obdir(m)= 1;
        isecs{s}.obtype(1,m)= edgetype*(sx>1) + 1*(sx==1);
        isecs{s}.obtype(2,m)= edgetype*(sx<N(1)) + 1*(sx==N(1));
    end
    if sx>1 && sy>1                     % down-left neighbor
        m= m+1;                         %  the left part
        isecs{s}.neighbors(m)= s-1-N(2);
        isecs{s}.obnodes(1,m)= obounds(3,s); 
        isecs{s}.obnodes(2,m)= obounds(3,s)+disp2corner(2)-edgetype;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + (obounds(1,s)-1)*(n(2)+1);
            isecs{s}.obdir(m)= -2;
            isecs{s}.obtype(1,m)= 1;
            isecs{s}.obtype(2,m)= edgetype;
        end
        m= m+1;                         %  the down part
        isecs{s}.neighbors(m)= s-1-N(2);
        isecs{s}.obnodes(1,m)= obounds(1,s)+1; 
        isecs{s}.obnodes(2,m)= obounds(1,s)+disp2corner(1)-edgetype;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1)+obounds(3,s);
            isecs{s}.obdir(m)= -1;
            isecs{s}.obtype(1,m)= 1 - 1*(isecs{s}.obnodes(1,m)==isecs{s}.obnodes(2,m) && edgetype==0);
            isecs{s}.obtype(2,m)= edgetype;
        end
    end
    if sx>1 && sy<N(2)                  % up-left neighbor
        m= m+1;                         %  the left part
        isecs{s}.neighbors(m)= s+1-N(2);
        isecs{s}.obnodes(1,m)= obounds(4,s)+1-disp2corner(2)+edgetype;
        isecs{s}.obnodes(2,m)= obounds(4,s)+1;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + (obounds(1,s)-1)*(n(2)+1);
            isecs{s}.obdir(m)= -2;
            isecs{s}.obtype(1,m)= edgetype;
            isecs{s}.obtype(2,m)= 1;
        end
        m= m+1;                         %  the up part
        isecs{s}.neighbors(m)= s+1-N(2);
        isecs{s}.obnodes(1,m)= obounds(1,s)+1; 
        isecs{s}.obnodes(2,m)= obounds(1,s)+disp2corner(1)-edgetype;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1)+obounds(4,s)+1;
            isecs{s}.obdir(m)= 1;
            isecs{s}.obtype(1,m)= 1 - 1*(isecs{s}.obnodes(1,m)==isecs{s}.obnodes(2,m) && edgetype==0);
            isecs{s}.obtype(2,m)= edgetype;
        end
    end
    if sx<N(1) && sy>1                  % down-right neighbor
        m= m+1;                         %  the right part
        isecs{s}.neighbors(m)= s-1+N(2);
        isecs{s}.obnodes(1,m)= obounds(3,s); 
        isecs{s}.obnodes(2,m)= obounds(3,s)+disp2corner(2)-edgetype;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + obounds(2,s)*(n(2)+1);
            isecs{s}.obdir(m)= 2;
            isecs{s}.obtype(1,m)= 0;
            isecs{s}.obtype(2,m)= edgetype;
        end
        m= m+1;                         %  the down part
        isecs{s}.neighbors(m)= s-1+N(2);
        isecs{s}.obnodes(1,m)= obounds(2,s)+1-disp2corner(1)+edgetype;
        isecs{s}.obnodes(2,m)= obounds(2,s);
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1)+obounds(3,s);
            isecs{s}.obdir(m)= -1;
            isecs{s}.obtype(1,m)= edgetype;
            isecs{s}.obtype(2,m)= 1 - 1*(isecs{s}.obnodes(1,m)==isecs{s}.obnodes(2,m) && edgetype==0);
        end
    end
    if sx<N(1) && sy<N(2)               % up-right neighbor
        m= m+1;                         %  the right part
        isecs{s}.neighbors(m)= s+1+N(2);
        isecs{s}.obnodes(1,m)= obounds(4,s)+1-disp2corner(2)+edgetype; 
        isecs{s}.obnodes(2,m)= obounds(4,s)+1;
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= isecs{s}.obnodes(:,m) + obounds(2,s)*(n(2)+1);        
            isecs{s}.obdir(m)= 2;
            isecs{s}.obtype(1,m)= edgetype;
            isecs{s}.obtype(2,m)= 0;
        end
        m= m+1;                         %  the up part
        isecs{s}.neighbors(m)= s+1+N(2);
        isecs{s}.obnodes(1,m)= obounds(2,s)+1-disp2corner(1)+edgetype; 
        isecs{s}.obnodes(2,m)= obounds(2,s);
        if isecs{s}.obnodes(2,m)<isecs{s}.obnodes(1,m)
            isecs{s}.obnodes(:,m)= [];
            isecs{s}.neighbors(m)= [];
            m= m-1;
        else
            isecs{s}.obnodes(:,m)= (isecs{s}.obnodes(:,m)-1)*(n(2)+1)+obounds(4,s)+1;
            isecs{s}.obdir(m)= 1;
            isecs{s}.obtype(1,m)= edgetype;
            isecs{s}.obtype(2,m)= 1 - 1*(isecs{s}.obnodes(1,m)==isecs{s}.obnodes(2,m) && edgetype==0);
        end
    end


end                                     % end of function partition



end

function ixbounds = part1d_simple(nx,Nx)
% leave the remainder to the last subdomain
    ixbounds= zeros(Nx,2);
    ixbounds(:,1)= 1+(0:Nx-1)'*floor(nx/Nx);
    ixbounds(:,2)= ixbounds(:,1) + floor(nx/Nx)-1;
    ixbounds(Nx,2)= nx;
    ixbounds= ixbounds';
end

function ixbounds = part1d(nx,Nx)
% split the remainder to one's and distribute to the first subdomains
    ixbounds= zeros(Nx,2);
    m= floor(nx/Nx);
    r= nx-Nx*m;
    if r~=0
        ixbounds(1:r,1)= 1+(0:r-1)*(m+1);
        ixbounds(r+1:end,1)= r*(m+1)+1+(0:Nx-r-1)*m;
    else
        ixbounds(:,1)= 1+(0:Nx-1)*m;        
    end    
    ixbounds(1:Nx-1,2)= ixbounds(2:Nx,1)-1;
    ixbounds(Nx,2)= nx;
    ixbounds= ixbounds';
end