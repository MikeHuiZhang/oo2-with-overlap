clear;
% do the scaling tests for the paper
npml = 8; Ny = 1; ox = 1; oy = 1; fun = 'MSS';

%    OO2
ictx = 'O2'; ictx2 = []; npmldd = 8; dw = 4*pi/3000;
prefix = 'outerpml8/cavityoo2';

% f = 4
f = 4; h1= pi/8/(2*pi*f/1500); Nx = 20; 
[soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
save([prefix,'f4.mat'],'resvec','errvec');

% % f = 8
% f = 8; h1= pi/8/(2*pi*f/1500); Nx = 40; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f16.mat'],'resvec','errvec'); pause(1);
% 
% % f = 16
% f = 16; h1= pi/8/(2*pi*f/1500); Nx = 80; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f16.mat'],'resvec','errvec'); pause(1);
%  
%  % f = 32
% f = 32; h1= pi/8/(2*pi*f/1500); Nx = 160; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f32.mat'],'resvec','errvec');

%   Zolotarev
ictx = 'O2s2'; ictx2 = []; npmldd = 8; dw = 2*pi/3000;
prefix = 'outerpml8/cavityzol';

% f = 4
f = 4; h1= pi/8/(2*pi*f/1500); Nx = 20; 
[soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
save([prefix,'f4.mat'],'resvec','errvec');

% % f = 8
% f = 8; h1= pi/8/(2*pi*f/1500); Nx = 40; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f8.mat'],'resvec','errvec');
% 
% % f = 16
% f = 16; h1= pi/8/(2*pi*f/1500); Nx = 80; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f16.mat'],'resvec','errvec');
% 
% % f = 32
% f = 16; h1= pi/8/(2*pi*f/1500); Nx = 80; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f16.mat'],'resvec','errvec');

%    PML
ictx = 'PML'; ictx2 = []; npmldd = 8;  dw = 2*pi/3000;
prefix = 'outerpml8/cavitypml';

% f = 4
f = 4; h1= pi/8/(2*pi*f/1500); Nx = 20; 
[soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
save([prefix,'f4.mat'],'resvec','errvec');

% % f = 8
% f = 8; h1= pi/8/(2*pi*f/1500); Nx = 40; 
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f8.mat'],'resvec','errvec');
% 
% % f = 16
% f = 16; h1= pi/8/(2*pi*f/1500); Nx = 80;
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f16.mat'],'resvec','errvec');
% 
% % f = 32
% f = 32; h1= pi/8/(2*pi*f/1500); Nx = 160;
% [soltime,ittime,oostime,iter,resvec,errvec,deltau] = helm2drand(f,npml,h1,Nx,Ny,ox,oy,fun,ictx,ictx2,npmldd,dw);
% save([prefix,'f32.mat'],'resvec','errvec');