function Rt = PartOfUnity(n,obounds,ibounds,nlayers,pufun)
%PartOfUnity -- Construct prolongation matrix corresponds to partition of
%unity
%
%  n -- n(1),n(2) number of elements in x,y directions
%
%  obounds -- [ixbegin, ixend, iybegin, iyend] for elements in this
%  overlapping subdomain
%
%  ibounds -- bounds for elements in non-overlapping domain, but ixend,
%  iyend have been further decreased by one
%
%  nlayers -- an integer, from non-overalapping, how many layers
%  counted towards interior until the nodes that take values one for
%  partition of unity, usually, where are the boundary of neighbors, less
%  than or equal to the overlap
%
%  pufun -- optinonal, function taking values 1 and zero from the interior
%  layer decided by nlayers to the artificial boundary of overlapping,
%  subdomain, described on the domain [0,1], and usually satisfies 
%  f(t)+f(1-t)=1; default linear function
%
%  OUTPUTS
%
%  Rt -- prolongation matrix that maps vector on overlapping subdomain to
%  the global domain


%   compute the [i,j,v] of Rt

% ii is global indices
ix= obounds(1):obounds(2)+1;
iy= obounds(3):obounds(4)+1;
[Ix Iy]= meshgrid(ix,iy);
ii= (Ix(:)-1)*(n(2)+1) + Iy(:);

% jj is local indices
nlocal= [obounds(2)-obounds(1)+1, obounds(4)-obounds(3)+1];
[Ix Iy]= meshgrid(1:nlocal(1)+1,1:nlocal(2)+1);
jj= (Ix(:)-1)*(nlocal(2)+1) + Iy(:);

% vv takes values 1 for nodes on or away from the overlapping region
% and takes values 0 on artificial boundary of the overlapping subdomain 
vv= ones(nlocal(2)+1,nlocal(1)+1);
if nargin<5 || isempty(pufun)
    pufun= @(t) 1-t;
end
for m= nlayers-1:-1:-nlayers
    t= (1-m/nlayers)*0.5;               % assume symmetric overlap from
                                        % non-overlapping interface
    val= pufun(t);
    if 1==1ibounds(1)
        ix1= 1;
    else                
        ix1= max(ibounds(1)+m,1)-obounds(1)+1;
    end
    if 1==ibounds(3)
        iy1= 1;
    else        
        iy1= max(ibounds(3)+m,1)-obounds(3)+1;
    end
    if ibounds(2)==n(1)
        ix2= n(1)+1-obounds(1)+1;
    else
        ix2= min(ibounds(2)+2-m,n(1)+1)-obounds(1)+1;
    end
    if ibounds(4)==n(2)
        iy2= n(2)+1-obounds(3)+1;
    else
        iy2= min(ibounds(4)+2-m,n(2)+1)-obounds(3)+1;
    end
    if ibounds(1)>1                     % left 
        vv(iy1:iy2,ix1)= val;
    end
    if ibounds(2)~=n(1)                 % right
        vv(iy1:iy2,ix2)= val;
    end
    if ibounds(3)>1                     % bottom
        vv(iy1,ix1:ix2)= val;
    end
    if ibounds(4)~=n(2);                % top
        vv(iy2,ix1:ix2)= val;
    end
end
vv= vv(:);


%   construction of sparse matrix Rt
Rt= sparse(ii,jj,vv,(n(2)+1)*(n(1)+1),length(vv));
