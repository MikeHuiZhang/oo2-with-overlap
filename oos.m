function [u,ittime,flag,relres,iter,resvec,maxerr,errvec,deltau]= oos(A,AA,iterin,b,n,N,o,prolongation,matctx,rhsctx,optctx,cpctx,funnames,itermethods,uinit,maxit,tol,udirect,optctx2,upml)
%oos -- overlapping optimized Schwarz methods
%
%  [u,flag,relres,iter,resvec]= oos(A,AA,b,n,N,o,prolongation,matctx,rhsctx,...
%  optctx,cpctx,funnames,itermethods,uinit,maxit,tol,udirect)
%
%  prolongation -- 'restricted','full','unity', describes the
%  prolongation operator for composing the global solution
%
%  matctx -- {a,w,p,tgv,h,bctype,popt,stretch,popt2,pmlctx,R,nall,pmlddctx}
%
%     a -- density array  
%
%     w -- wavenumber array, constant for each element of the global domain
%
%     p -- exterior Robin-like parameters, [p1,p2,p3], as function of w
%  
%     tgv -- 1 or large number, if 1, first zero rows for Dirichlet nodes
%     then put 1 on diagonal, if large number, only put tgv on diagonal  
%
%     h -- h(1),h(2)
%
%     bctype -- of the original problem
%
%     popt -- for Robin on interfaces
%
%     stretch -- c.f. strchp.m rescale popt for different subdomains
%
%     popt2 -- used for two-level Robin parameters
%
%     pmlctx -- pml context for the original problem
%
%     R -- matrix of restriction to physical nodes in the original PML augmented
%     domain
%
%     nall -- number of elements including PML along x,y
%
%     pmlddctx -- pml context for domain decomposition
%
%  rhsctx -- {domain,scpoints,Sp,sclines,Sl,Sa}, context for re-assembly of
%  rhs at interface nodes, can be empty to indicate no re-assembly and a 0-1
%  restriction of the global rhs will be used
%       
%     domain -- (xl,xr;yl,yr) of the global domain
% 
%     scpoints -- (x,y) of point sources
%
%     Sp -- strength of point sources
% 
%     sclines -- line sources, (x1,y1,x2,y2) beginning and ending points
%
%     Sl -- strength of line sources
%
%     Sa -- area source function
%
%  The rhs at non-Dirichlet interface can be arbitrary, because we can put
%  part of sources into lambda.
% 
%  c.f. getpopt.m for structure of optctx
%
% TODO: right preconditioning
%
% Comments on the coarse problem.
%
% Substructured: F l = d
% 
% We split l = l + Q*mu, then 
%
%    F l + F Q mu = d,
%
% Apply the projector I - F Q (Q'FQ)^-1 Q', we obtain
%
%    (I - F Q (Q'FQ)^-1 Q') F l = (I - F Q (Q'FQ)^-1 Q') d.
% 
% Although it is singular, but it essentially lies in the subspace that is
%  to the column space of Q.  For recovery of the original l, note
% that 
% 
%    F Q mu = d - F l,
%
% and apply Q' to obtain 
%
%    (Q' F Q) mu = Q' (d - F l).



%   initialization
global resu;                            % residual of u in substructured
global erru;                            % error of u to udirect
global deltau;                          % deltau(n) = |u_n-u_{n-1}|/|u_n|
global oldu;
global Arows Arows2;
resvo=[]; resvp=[]; resu= []; erru= []; deltau= []; oldu= []; Arows = []; Arows2 = [];
if iscell(itermethods)
    niter= length(itermethods);
else
    niter= 1;
end
if iscell(funnames)
    nfun= length(funnames); 
else
    nfun= 1;
end
if nfun>niter
    error('wrong arg',['length of funnames must be less than that of ' ...
                       'itermethods']);
end
if isempty(cpctx) || isempty(cpctx{1})
    nc= 0;
else
    nc= cpctx{1};
    if length(cpctx)<6 || isempty(cpctx{6})
        coarse= 'Ac2';
    else
        coarse= cpctx{6};
    end
end
h= matctx{5}; bctype= matctx{6};
if length(matctx)>11 && ~isempty(find(bctype==4,1))
    pmlctx= matctx{10}; R= matctx{11}; nall= matctx{12};
    numpml.orig= pmlctx.nl*(bctype==4);
else
    pmlctx= []; R= []; nall= n; numpml.orig= [0 0 0 0];
end
switch prolongation
  case 'full',
    prolongtype= 1;
  case 'unity'
    prolongtype= 2;
  otherwise,
    prolongtype= 0;
end

%   partition
[ibounds,ibounds2,obounds,isecs]= partition(n,N,o,bctype);

%   subdomain matrices, ready for volume Schwarz
Ns= N(1)*N(2);
Ro= cell(Ns,1); Rt= Ro; As= Ro; Asf= As; Rt2= Rt; Ro2= Ro; As2= As;
Aneum= As; Aaug= As; numpml.origs= zeros(4,Ns); numpml.dds= zeros(4,Ns);
for s= 1:Ns
    [sy, sx]= ind2sub([N(2),N(1)],s);
    pobound= obounds(:,s);              % bounds of subdomain elements in the
                                        % original pml-augmented domain
    if nall==n
        Ro{s}= restriction(n,obounds(:,s));
    else
        pobound= addpml2bounds(pobound,N,sx,sy,pmlctx.nl,bctype);
        Ro{s}= restriction(nall,pobound);
    end
    % source transfer forward needs a right overlap zeroed restriction of source
    if ~isempty(strfind(cell2mat(funnames),'MSS0'))
        o2b= pobound;
        if sx<N(1)
            o2b(2)= o2b(2)-2*o(1);            
        end
        if sy<N(2)
            o2b(4)= o2b(4)-2*o(2);
        end
        Ro2{s}= restriction(nall,o2b);
        Ro2{s}= Ro{s}*(Ro2{s}'*Ro2{s});
    else
        Ro2{s}= Ro{s};
    end
    if 0==prolongtype
        pibound= ibounds(:,s);
        if ~isempty(pmlctx);
            pibound= addpml2bounds(pibound,N,sx,sy,pmlctx.nl,bctype);
        end
        Ris= restriction(nall,pibound);
        Rt{s}= Ris'*(Ris*(Ro{s})');
        if ~isempty(strfind(cell2mat(funnames),'MSS')) && max(o)==0
            pibound2= ibounds2(:,s);
            if ~isempty(pmlctx);
                pibound2= addpml2bounds(pibound2,N,sx,sy,pmlctx.nl,bctype);
            end            
            Ris2= restriction(nall,pibound2);
            Rt2{s}= Ris2'*(Ris2*(Ro{s})');
        else
            Rt2{s}= Rt{s};
        end
    elseif 1==prolongtype
        Rt{s}=Ro{s}';
        Rt2{s}= Rt{s};
    else
        if nall==n
            Rt{s}= PartOfUnity(n,obounds(:,s),ibounds(:,s),min(o));
            % for test only
            Ris= restriction(n,ibounds(:,s));
            Rtr{s}= Ris'*(Ris*(Ro{s})');
        else
            pibound= ibounds(:,s);
            pibound= addpml2bounds(pibound,N,sx,sy,pmlctx.nl,bctpye);
            Rt{s}= PartOfUnity(nall,pobound,pibound,min(o));
            Rt2{s}= Rt{s};
            % for test only
            Ris= restriction(nall,pibound);
            Rtr{s}= Ris'*(Ris*(Ro{s})');
        end
    end
    if ~isempty(AA)
        As{s}= Ro{s}*AA*(Ro{s})';
    else
        As{s}= Ro{s}*A*(Ro{s})';        
    end
    if exist('optctx2','var') && ~isempty(optctx2) && ~isempty(optctx2{8})
        As2{s}= As{s};
    end
    [Rom{s},As{s},Rp{s},Aneum{s},Aaug{s},numpml.origs(:,s),numpml.dds(:,s)]= modify(Ro{s},As{s},nall,pobound,matctx,optctx,[sx sy],N,n,obounds(:,s),A);
    Asf{s}= factorize(As{s});
    if exist('optctx2','var') && ~isempty(optctx2) && ~isempty(optctx2{8})
        [Ro2m{s},As2{s},Rp2{s}]= modify(Ro2{s},As2{s},nall,pobound,matctx,optctx2,[sx sy],N,n,obounds(:,s),A);
        Asf2{s}= factorize(As2{s});
    else
        As2{s}= As{s}; Rp2{s}= Rp{s}; Asf2{s}= Asf{s}; Ro2m{s}= Rom{s};
    end    
    if ~isempty(Rp{s})
        Ro{s}= Rp{s}'*Ro{s};
        Rom{s}= Rp{s}'*Rom{s};
        Rt{s}= Rt{s}*Rp{s};
    end
    if ~isempty(Rp2{s})
        Rt2{s}= Rt2{s}*Rp2{s};
        Ro2{s}= Rp2{s}'*Ro2{s};
        Ro2m{s}= Rp2{s}'*Ro2m{s};
    end
end

%   interface matrices, maps between lambda and subdomains
needBmat= 0; B= []; Bt= []; bs= []; linit= [];
if iscell(funnames)
    for m= 1:nfun
        if ~isempty(strfind(funnames{m},'sub')) || nc
            needBmat= 1;
            break;
        end
    end
elseif ischar(funnames)
    if ~isempty(strfind(funnames,'sub')) || nc
        needBmat= 1;
    end
end
if needBmat
    if exist('optctx2','var') && ~isempty(optctx2)
        warning('oos: optctx2 is non-empty but will not be used for substructured');
    end
% $$$     [B,Bt,iil]= Bmat(n,obounds,isecs,max(o)>0,As,Aneum,Aaug,numpml,'split');
    [B,Bt,iil]= Bmat(n,obounds,isecs,max(o)>0,   Aneum,Aaug,numpml,Rp);
    if isempty(rhsctx)
        bs= modfrhs(b,Rom);             % here should use Rom not Ro
    elseif isempty(rhsctx) && max(o)<1
        bs= modfrhs(b,Rom,Rp,obounds,n,numpml);
    else
        bs= modfrhs(b,Rom,Rp,obounds,n,numpml,h,rhsctx);
    end
    linit= zeros(size(Bt{1},2),1);
    for s= 1:Ns
        us= Ro{s}*uinit;                % here should use Ro not Rom
        linit= linit+Bt{s}'*(As{s}*us-bs{s});
    end
end


%   coarse basis
if nc
    if strcmp(cpctx{3},'subdomain')
        Qc= getQc(cpctx,matctx,n,N,obounds,iil,Rom);
    else
        Qc= getQc(cpctx,matctx,n,N,obounds,iil,Bt,Rp);% get interface basis
    end
end


%   for each of funnames and itermethods
flag= zeros(nfun,1); relres= flag; iter= zeros(nfun,2); 
if exist('udirect','var') && ~isempty(udirect) && nargout>4
    maxerr= flag;
end
if nfun>1
    resvec= cell(nfun);
end
for ifun= 1:nfun
    if iscell(itermethods)
        itermeth= itermethods{ifun};
    else
        itermeth= itermethods;
    end
    if iscell(funnames)
        myfun= funnames{ifun};
    else
        myfun= funnames;
    end
    switch itermeth
      case {'gmres','mygmres'},
        switch myfun
          case 'AS',
            Pb= AS(zeros(size(b)),b,A,Rom,Rt,Asf,AA,iterin);
            PA= @(u) u-AS(u,[],A,Rom,Rt,Asf,AA,iterin);
% $$$             P= @(r) ASP(r,Rom,Rt,As,AA,iterin);
            if nc
                if strcmp(cpctx{3},'subdomain')
                    C= Qc;
                else
                    [~,~,C]= ASsub(Qc,Asf,[],B,Bt,Rt);
                end
                switch coarse
                  case 'PAc',
                    Ac= C'*PA(C);
                    Ac= factorize(Ac);
                    xtemp= C*(Ac\(C'*Pb));
                    CPb= Pb-PA(xtemp);
                  case 'Ac1',
                    Ac= C'*A*C;
                    Ac= factorize(Ac);
                    uc= C*(Ac\(C'*b));
                    res= A*Pb;
                    CPb= Pb-C*(Ac\(C'*res));
                    res= A*uinit;
                    uinitc= uinit - C*(Ac\(C'*res)) + uc;
                  case 'Ac2',
                    Ac= C'*A*C;
                    Ac= factorize(Ac);
                    res= b-A*Pb;
                    uc= C*(Ac\(C'*res));
                    CPb= AS(uc,[],A,Rom,Rt,Asf);
                    CPb= CPb+Pb+AS(Pb,[],A,Rom,Rt,Asf);
                  case 'Ac2a',
                    Ac= C'*A*C;
                    Ac= factorize(Ac);
                    uc= C*(Ac\(C'*b));
                    CPb= AS(uc,[],A,Rom,Rt,Asf);
                    CPb= CPb+Pb;
                  case 'Ac2b',
                    Ac= C'*A*C;
                    Ac= factorize(Ac);
                    res= b-A*Pb;
                    CPb= Pb+C*(Ac\(C'*res));
                end
            end
          case 'MS',
            Pb= MS(zeros(size(b)),b,A,Rom,Rt,Asf,0,[],AA,iterin);
            PA= @(u) u-MS(u,zeros(size(b)),A,Rom,Rt,Asf,0,[],AA,iterin);
          case {'MSS','MSS0'}           % symmetric
            Pb= MS(zeros(size(b)),b,A,Rom,Rt,Asf,1,Rt2,AA,iterin,Ro2m,Asf2);
            PA= @(u) u-MS(u,zeros(size(b)),A,Rom,Rt,Asf,1,Rt2,AA,iterin,Ro2m,Asf2);
          case 'MSSo',
            Pb= MSo(zeros(size(b)),b,A,Rom,Asf,1);
            PA= @(u) u-MSo(u,zeros(size(b)),A,Rom,Asf,1);
          case 'MSC',                   % colored
            Pb= MSC(zeros(size(b)),b,A,Rom,Rt,Asf,N);
            PA= @(u) u-MSC(u,zeros(size(b)),A,Rom,Rt,Asf,N);
          case 'ASsub'
            [d,~,ub]= ASsub(zeros(size(Bt{1},2),1),Asf,bs,B,Bt,Rt);
            resu = []; erru = []; deltau = []; oldu = [];
            F= @(l) l-ASsub(l,Asf,[],B,Bt,Rt);
            l2u= @(l) ASsub(l,Asf,bs,B,Bt,Rt);
            if nc
                Fc= Qc'*(Qc-ASsub(Qc,Asf,[],B,Bt));
                Fc= factorize(Fc);
                ltemp= Qc*(Fc\(Qc'*d));
                Pd= d-ltemp+ASsub(ltemp,Asf,[],B,Bt); 
            end    
          case 'MSsub'
            [d,~,ub]= MSsub(zeros(size(Bt{1},2),1),Asf,bs,B,Bt,Rt);
            F= @(l) l-MSsub(l,Asf,[],B,Bt,Rt);
          case 'MSSsub'
            [d,~,ub]= MSSsub(zeros(size(Bt{1},2),1),Asf,bs,B,Bt,Rt);
            F= @(l) l-MSSsub(l,Asf,[],B,Bt,Rt);
          case 'MSCsub'
            [d,~,ub]= MSCsub(zeros(size(Bt{1},2),1),Asf,bs,B,Bt,Rt,N);
            F= @(l) l-MSCsub(l,Asf,[],B,Bt,Rt,N);
        end
        itbegin= tic;
        if isempty(strfind(myfun,'sub'))
            if ~nc
                switch itermeth
                  case 'gmres',
                    % Note: today MATLAB's gmres uses the preconditioned residual, so pass PA or P
                    % to gmres makes no difference!
                    if exist('P','var') && ~isempty(P)
                        [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                            gmres(A,b,[],tol,maxit,P,[],uinit);
                    else
                        [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                            gmres(PA,Pb,[],tol,maxit,[],[],uinit);
                    end
                  case 'mygmres',
                      % to have residuals concentrated around interfaces,
                      % use initial guess Pb instead of uinit
                      if exist('upml','var') && ~isempty(upml)
                        [u,flag(ifun),relres(ifun),iter(ifun,:),rsv,erru,resvo,resvp]= ...
                           mygmres(PA,Pb,[],tol,maxit,[],[],uinit,A,b,[],R,udirect,upml);
                      else
                          [u,flag(ifun),relres(ifun),iter(ifun,:),rsv,erru,resvo]= ...
                             mygmres(PA,Pb,[],tol,maxit,[],[],uinit,A,b,[],R,udirect);
                      end
                end
            else
                switch coarse
                  case 'PAc',
                    [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                        gmres(@CPA,CPb,[],tol,maxit,[],[],uinit);
                    res= C'*(Pb-PA(u));
                    u= u + C*(Ac\res);
                  case 'Ac1',
                    [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                          gmres(@PAc1,CPb,[],tol,maxit,[],[],uinitc);
                  case 'Ac2',
                    [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                        gmres(@PAc2,CPb,[],tol,maxit,[],[],uinit);
                  case 'Ac2a',
                    [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                        gmres(@PAc2a,CPb,[],tol,maxit,[],[],uinit);
                  case 'Ac2b',
                    [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                        gmres(@PAc2b,CPb,[],tol,maxit,[],[],uinit);
                end
            end
        else                            % substructured
            if ~nc
                switch itermeth
                  case 'mygmres',
                    meshx= 0:h(1):n(1)*h(1);
                    meshy= 0:h(2):n(2)*h(2);
                    resu= []; erru= []; oldu= []; deltau= [];
                    [l,flag(ifun),relres(ifun),iter(ifun,:),rsv,erru,resvo]= ...
                        mygmres(F,d,[],tol,maxit,[],[],linit,A,b,l2u,R,udirect);
                  otherwise,
                    resu= []; erru= []; oldu= []; deltau= [];
                    [l,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                        gmres(F,d,[],tol,maxit,[],[],linit);
                end
            else
                [l,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                    gmres(@PF,Pd,[],tol,maxit,[],[],linit);
                res= Qc'*(d-F(l));
                mu= Fc\res;
                l= l+Qc*mu;
            end
            switch myfun
              case 'MSCsub',
                [~,~,u]= MSCsub(l,Asf,[],B,Bt,Rt,N);                
              otherwise,
                myfunc= str2func(myfun);
                [~,~,u]= myfunc(l,Asf,[],B,Bt,Rt);
            end
            u= u + ub;
        end
        disp('gmres time: '); ittime= toc(itbegin)
      case 'richardson',
        switch myfun
          case 'AS',
            iterfun= @(u) AS(u,b,A,Rom,Rt,Asf,AA,iterin);
          case 'MS',
            iterfun= @(u) MS(u,b,A,Rom,Rt,Asf,0,[],AA,iterin);
          case 'MScompare',
            %iterfun= @(u) MSo(u,b,A,Rom,Asf);
            iterfun= @(u) MS(u,b,A,Rom,Rt,Asf);
            iterfunr= @(u) MS(u,b,A,Rom,Rtr,Asf);
          case {'MSS','MSS0'}
            iterfun= @(u) MS(u,b,A,Rom,Rt,Asf,1,Rt2,AA,iterin,Ro2m,Asf2);
          case 'MSScompare',
            %iterfun= @(u) MSo(u,b,A,Rom,Asf,1);
            iterfun= @(u) MS(u,b,A,Rom,Rt,Asf,1);
            iterfunr= @(u) MS(u,b,A,Rom,Rtr,Asf,1);            
          case 'MSC',
            iterfun= @(u) MSC(u,b,A,Rom,Rt,Asf,N);            
          case 'ASsub'
            iterfun= @(l) ASsub(l,Asf,bs,B,Bt,Rt,A,b,[],udirect,R);
          case 'MSsub'
            iterfun= @(l) MSsub(l,Asf,bs,B,Bt,Rt,A,b);
          case 'MSSsub'
            iterfun= @(l) MSSsub(l,Asf,bs,B,Bt,Rt,A,b);
          case 'MSCsub'
            iterfun= @(l) MSCsub(l,Asf,bs,B,Bt,Rt,N,A,b);
        end
        itbegin= tic;
        if isempty(strfind(myfun,'sub'))
            resfun= @(u) norm(b-A*u);
            [u,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
                richardson(resfun, uinit, iterfun, maxit, tol);
            switch myfun
              case {'MScompare','MSScompare'}
                ur= richardson(resfun, uinit, iterfunr, maxit, tol);
                err= u-ur; err= reshape(err,n(2)+1,n(1)+1);
                figure, surf(real(err));                
            end
        else                            % substructured
            resu=[]; erru=[];
            [l,flag(ifun),relres(ifun),iter(ifun,:),rsv]= ...
              richardson([], linit, iterfun, maxit,tol);
            switch myfun
              case 'MSCsub',
                [~,~,u]= MSCsub(l,Asf,bs,B,Bt,Rt,N,A,b);
              case 'ASsub',
                [~,~,u]= ASsub(l,Asf,bs,B,Bt,Rt,A,b,[],udirect,R);
              otherwise,
                myfunc= str2func(myfun);
                [~,~,u]= myfunc(l,Asf,bs,B,Bt,Rt,A,b);
            end
            rsv= resu;
        end
        disp('Richardson time'); ittime = toc(itbegin)
    end
    if exist('udirect','var') && ~isempty(udirect) && nargout>4
        if isempty(R)
            maxerr(ifun)= max(abs(udirect(:)-u));
        else
            maxerr(ifun)= max(abs(udirect(:)-R*u));
        end
    end
    if nfun>1
        resvec{ifun}= rsv;
        if ~isempty(resvp) && ~isempty(resvo)
            resvec{ifun}= [rsv(:),resvo(:),resvp(:)];
        elseif ~isempty(resvo)
            resvec{ifun}= [rsv(:),resvo(:)];
        end
        errvec{ifun}= erru;
    else
        resvec= rsv;
        if ~isempty(resvp) && ~isempty(resvo)
            resvec= [rsv(:),resvo(:),resvp(:)];
        elseif ~isempty(resvo)
            resvec= [rsv(:),resvo(:)];
        end
        errvec= erru;
    end
end

function re= PF(l)
    re= F(l);
    re= l-Qc*(Fc\(Qc'*re));
    re= F(re);                          % F(I-QcFc^-1Qc'F)=(I-FQcFc^-1Qc')F
end                                     % end embedded function PF

function re= CPA(u)
    re= PA(u);
    re= u-C*(Ac\(C'*re));
    re= PA(re);
end                                     % end embedded function CPA

function re= PAc1(u)                    % (I-P0^-1A)P^-1A(I-P0^-1A)
    re= PA(u);                          % assume u is in range of I-P0^-1A
    res= A*re;
    re= re-C*(Ac\(C'*res));
end                                     % end embedded function PAc1
    
function re= PAc2(u)                    % I-(I-P^-1A)(I-P0^-1A)(I-P^-1A)
    re= u-PA(u);
    res= A*re;
    re= re-C*(Ac\(C'*res));
    re= re-PA(re);
    re= u-re;
end                                     % end embedded function PAc2

function re= PAc2a(u)                    % I-(I-P^-1A)(I-P0^-1A)
    res= A*u;
    re= u-C*(Ac\(C'*res));
    re= re-PA(re);
    re= u-re;
end                                     % end embedded function PAc2a

function re= PAc2b(u)                    % I-(I-P0^-1A)(I-P^-1A)
    re= u-PA(u);
    res= A*re;
    re= re-C*(Ac\(C'*res));
    re= u-re;
end                                     % end embedded function PAc2b
    
    
end                                     % end of function oos
    

function [u,flag,relres,iter,resvec]= richardson(resfun,uinit,iterfun,maxit,tol)
    global resu erru deltau oldu;
    deltau = []; oldu = []; resu = []; erru = [];
    u= uinit;
    resvec= zeros(maxit+1,1); 
    if ~isempty(resfun)
        resvec(1)= resfun(u);
    else
        resvec(1)= 1;                   % meaningless value
    end
    flag= 0;
    for iter= 1:maxit
        [u,resvec(iter+1)]= iterfun(u);
        relres= resvec(iter+1)/resvec(1);
% $$$         relres= resu(end)/resu(1);
% $$$         if ~isempty(deltau) && deltau(end)<tol
        if relres<tol
            break;
        end
    end
    if iter==maxit
        flag= 1;
    end
    resvec= resvec(1:iter+1);
    iter= [1 iter];
end                                     % end of function richardson