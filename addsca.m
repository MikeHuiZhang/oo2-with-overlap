function b=addsca(b,ix,iy,n,h,xl,yl,Sa)
    inode1= (ix-1)*(n(2)+1) + iy;
    [gp,gw]= getgpw2d();
    J= h(1)*h(2)/4;                     % area ratio to [-1,1]^2
    for m= 1:length(gw)
        xx= xl+(ix-1)*h(1)+(gp(m,1)+1)/2*h(1);
        yy= yl+(iy-1)*h(2)+(gp(m,2)+1)/2*h(2);
        b(inode1)= b(inode1)+J*gw(m)*Sa(xx,yy)*(1-gp(m,1))/2*(1-gp(m,2))/2;
        b(inode1+1)= b(inode1+1)+J*gw(m)*Sa(xx,yy)*(1-gp(m,1))/2*(1+gp(m,2))/2;
        b(inode1+n(2)+1)= b(inode1+n(2)+1)+J*gw(m)*Sa(xx,yy)*(1+gp(m,1))/2*(1- ...
                                                          gp(m,2))/2;
        b(inode1+n(2)+2)= b(inode1+n(2)+2)+J*gw(m)*Sa(xx,yy)*(1+gp(m,1))/2* ...
            (1+gp(m,2))/2;
    end
end
