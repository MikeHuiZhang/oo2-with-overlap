function svec = zolnew(n,c,d)
%ZOLNEW gives the square-rooted interpolation points of the Zolotarjov rational
% approximation P_{n-1}(z)/Q_n(z) of the square-root function s=sqrt(z).  In
% terms of 's', this function gives all the zeros of Z_{2*n}(s) such that max_s
% |Z(s)/Z(-s)| is minimized.  We have the relations Z(s) = -s*P(s^2) + Q(s^2)
% and |(P/Q - s) / (P/Q + s)| = |Z(s)/Z(-s)|, c.f. Druskin-Guttel-Knizherman
% ``Near-optimal perfectly matched layers for indefinite Helmholtz problems''.
%
% svec = zolnew(n,c,d) is for the approximation of 's' on the interval [c,d]
% (d>c>0) and the output `svec' of length 2*n consists of square-root 's' values
% for which P(z)/Q(z) interpolates s=sqrt(z).
if nargin<3
    error('zolnew: three arguments are neeeded but receives less.\n');
end
if n<=0
    svec = [];
    warning('zolnew: the 1st arg is zero, return empty');
    return;
end
if c>=d
    error('zolnew: the 2nd and 3rd arguments c,d must define an interval [c,d]\n');
end
if c<=0
    error('zolnew: the 2nd argument must be positive\n');
end

m = 2*n;
K = ellipke(1-c^2/d^2);
for j = 1:m
    [~,~,dn] = ellipj(K*(2*m-2*j+1)/(2*m),1-c^2/d^2);
    svec(j) = d*dn;
end